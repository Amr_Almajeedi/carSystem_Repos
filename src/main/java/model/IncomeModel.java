package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class IncomeModel {

	private String carNumber;
	private int driverNationalId;
	private String date;
	private String incomType;
	private double revenue;
	private Connection conn=null;
	
	public IncomeModel(){}

	/**
	 * @return the carNumber
	 */
	public String getCarNumber() {
		return carNumber;
	}

	/**
	 * @param carNumber the carNumber to set
	 */
	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	/**
	 * @return the driverNationalId
	 */
	public int getDriverNationalId() {
		return driverNationalId;
	}

	/**
	 * @param driverNationalId the driverNationalId to set
	 */
	public void setDriverNationalId(int driverNationalId) {
		this.driverNationalId = driverNationalId;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the incomType
	 */
	public String getIncomType() {
		return incomType;
	}

	/**
	 * @param incomType the incomType to set
	 */
	public void setIncomType(String incomType) {
		this.incomType = incomType;
	}

	/**
	 * @return the revenue
	 */
	public double getRevenue() {
		return revenue;
	}

	/**
	 * @param revenue the revenue to set
	 */
	public void setRevenue(double revenue) {
		this.revenue = revenue;
	}
	
	public int addNewIncome(String carNumber,int driverNationalId,String date,
			String incomType,double revenue)throws SQLException{
			
		conn=DBconnection.getActiveConnection();
		String sql="insert into income values(?,?,?,?,?)";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setString(1,carNumber);
		stmt.setInt(2,driverNationalId);
		stmt.setString(3,date);
		stmt.setString(4,incomType);
		stmt.setDouble(5,revenue);
		
		return stmt.executeUpdate();
	}
	
public ArrayList<IncomeModel> IncomeReport(String carNumber)throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="select * from income where carNumber=?";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setString(1,carNumber);
		
		ResultSet rs=stmt.executeQuery();
		ArrayList<IncomeModel>array=new ArrayList<IncomeModel>();
		IncomeModel obj=new IncomeModel();
		
		while (rs.next()){
			obj.setCarNumber(rs.getString("carNumber"));
			obj.setDriverNationalId(rs.getInt("driverNationalId"));
			obj.setDate(rs.getDate("date_").toString());
			obj.setIncomType(rs.getString("incomeType"));
			obj.setRevenue(rs.getDouble("revenue"));
		
			array.add(obj);
		}
		
		return array;
	}


	public double totalIncome(String carNumber)throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="select sum(revenue) from income where carNumber=?)";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setString(1,carNumber);
		
		ResultSet rs=stmt.executeQuery();
		double amount=-1;
		
		while (rs.next()){
			amount=rs.getDouble("revenue");
			
		}
		
		return amount;
	}
	
	public double employeeTotalIncome(int nationalId)throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="select sum(revenue) from income where driverNationalId=?)";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setInt(1,nationalId);
		
		ResultSet rs=stmt.executeQuery();
		double amount=-1;
		
		while (rs.next()){
			amount=rs.getDouble("revenue");
			
		}
		return amount;
	}
	public double dayTotalIncome(String date)throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="select sum(revenue) from income where date_=?)";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setString(1,date);
		ResultSet rs=stmt.executeQuery();
		double amount=-1;
		
		while (rs.next()){
			amount=rs.getDouble("revenue");
			
		}
		
		return amount;
	}
public double dayIncome(String carNumber,String date)throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="select sum(revenue) from income where date_=? and carNumber=?)";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setString(1,date);
		stmt.setString(1,carNumber);
		
		ResultSet rs=stmt.executeQuery();
		double amount=-1;
		
		while (rs.next()){
			amount=rs.getDouble("revenue");
			
		}
		
		return amount;
	}
	
	public double IncomeForPeroid(String carNumber,String fromDate,String toDate)throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="select sum(revenue) from income where carNumber=? and date_ between(?,?)";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setString(1,carNumber);
		stmt.setString(2,fromDate);
		stmt.setString(3,toDate);
		
		ResultSet rs=stmt.executeQuery();
		double amount=-1;
		
		while (rs.next()){
			amount=rs.getDouble("revenue");
			
		}
		
		return amount;
	}

public double IncomeFromType(String carNumber,String type, String date)throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="select sum(revenue) from income where carNumber=? and incomeType=? and date_=?";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setString(1,carNumber);
		stmt.setString(2,type);
		stmt.setString(2,date);
		
		ResultSet rs=stmt.executeQuery();
		double amount=-1;
		
		while (rs.next()){
			amount=rs.getDouble("revenue");
			
		}
		
		return amount;
	}

}
