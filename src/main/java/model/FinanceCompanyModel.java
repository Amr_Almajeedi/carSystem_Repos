package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class FinanceCompanyModel {

	private String companyName;
	private int numberOfCars;
	private Connection conn=null;
	
	public FinanceCompanyModel(){
		
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * @return the numberOfCars
	 */
	public int getNumberOfCars() {
		return numberOfCars;
	}

	/**
	 * @param numberOfCars the numberOfCars to set
	 */
	public void setNumberOfCars(int numberOfCars) {
		this.numberOfCars = numberOfCars;
	}
	
	public ArrayList<FinanceCompanyModel> getAllFinanceCompanies() throws SQLException{
		ArrayList<FinanceCompanyModel> companies = new ArrayList<FinanceCompanyModel>();
		
		conn=DBconnection.getActiveConnection();
		String sql="select * from financecompany";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		ResultSet rs=stmt.executeQuery();
		
		while(rs.next()){
			FinanceCompanyModel obj = new FinanceCompanyModel();
			obj.setCompanyName(rs.getString("companyName"));
			obj.setNumberOfCars(rs.getInt("numberOfCars"));
			
			companies.add(obj);  
		}
		
		return companies;
	}
	
	public FinanceCompanyModel getFinanceCompany(String companyName) throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="select * from financecompany where companyName=?";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setString(1,companyName);
		ResultSet rs=stmt.executeQuery();
		FinanceCompanyModel obj = new FinanceCompanyModel();
		while(rs.next()){
			obj.setCompanyName(rs.getString("companyName"));
			obj.setNumberOfCars(rs.getInt("numberOfCars"));
			
			return obj;
		}
		return null;
	}
	
	public int addNewFinanceCompany(String companyName,int numberOfCars) throws SQLException{
			
		if(getFinanceCompany(companyName) == null){
			conn=DBconnection.getActiveConnection();
			String sql="insert into financecompany values(?,?)";
			PreparedStatement stmt=conn.prepareStatement(sql);
			
			stmt.setString(1,companyName);
			stmt.setInt(2,numberOfCars);
			
			return stmt.executeUpdate();
			//return ""+conn;
		}
		return 0;
	}
	
public FinanceCompanyModel financeCompanyReport() throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="select * from financecompany";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		ResultSet rs=stmt.executeQuery();
		
		FinanceCompanyModel obj=new FinanceCompanyModel();
		
		while (rs.next()){
			obj.setCompanyName(rs.getString("companyName"));
			obj.setNumberOfCars(rs.getInt("numberOfCars"));
			
		}
		
		return obj;
}
		public int getNumberOfCars(String name) throws SQLException{
			
			conn=DBconnection.getActiveConnection();
			String sql="select * from financecompany where companyName=?";
			PreparedStatement stmt=conn.prepareStatement(sql);
			
			stmt.setString(1,name);
			
			ResultSet rs=stmt.executeQuery();
			
			FinanceCompanyModel obj=new FinanceCompanyModel();
			int num=-1;
			while (rs.next()){
				
				num= rs.getInt("numberOfCars");
				
			}
			
			return num;
		}
		
		public int  updateNumberOfCars(String name,int num) throws SQLException{
			
			if(getFinanceCompany(name) != null){
				conn=DBconnection.getActiveConnection();
				String sql="update financecompany set numberOfCars=? where companyName=?";
				PreparedStatement stmt=conn.prepareStatement(sql);
				
				stmt.setInt(1,num);	
				stmt.setString(2,name);
				
				return stmt.executeUpdate();
			}
			return 0;
		}
	
	
}
