 package model;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.ws.rs.FormParam;

import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;

public class CarModel {

	private String carNumber;
	private String type;
	private String model;
	private String style;
	private String color;
	private int carDriverNationalId;
	private double countKm;
	private double monthlyInstallment;
	private double totalLoan;
	private double remainLoan;
	private String firstInstallmentDate;
	private String carStatus;
	private String paymentType;
	private String carCompanyName;
	private String imageUrl;
	private double totalFines;
	private int assigned_not;
	
	private Connection conn=null;
	public CarModel(){
		
	}
	
	public  byte[] getImageData(String id)
    {
             byte[] fileBytes = null;
             
             Connection conn=DBconnection.getActiveConnection();
             String query;
             try
             {
                     query = "select stream from car where carNumber='" + id+"'"; 
                     Statement state = conn.createStatement();
                     ResultSet rs = state.executeQuery(query);
                     while (rs.next())
                    {
                        fileBytes = rs.getBytes("stream"); 
                      //  obj.bytes = fileBytes;
                       // all.add(obj);
                    }        
                    //return all;
             }
             catch (Exception e)
             {
                     e.printStackTrace();
             }
             //return null;
             return fileBytes;
    }
	
	public void getImageData()
    {
		Connection con=DBconnection.getActiveConnection();
             
             byte[] fileBytes;
             String query;
             try
             {
                     query = "select * from contacts";
                     Statement state = conn.createStatement();
                     ResultSet rs = state.executeQuery(query);
                     if (rs.next())
                    {
                    	 Blob blob = rs.getBlob("photo");
                          fileBytes=blob.getBytes(1,(int)blob.length());
                          OutputStream targetFile=  
                          new FileOutputStream("C://new.png");

                          targetFile.write(fileBytes);
                          targetFile.close();
                    }        
                     
             }
             catch (Exception e)
             {
                 e.printStackTrace();
             }
    }
	
	public String printImage(){
		try{
            //Class.forName("com.mysql.jdbc.Driver");
            //Connection con=DriverManager.getConnection("jdbc:mysql://localhost/demo","root","root");
        	Connection con=DBconnection.getActiveConnection();
            
        	File file=new File("E:\\image1.png");
            //FileOutputStream fos=new FileOutputStream(file);
            byte b[];
            Blob blob;
            InputStream in ;
            
            PreparedStatement ps=con.prepareStatement("select * from contacts"); 
            ResultSet rs=ps.executeQuery();
            /*
             * InputStream in = blob.getBinaryStream();
             * */
            while(rs.next()){
                blob=rs.getBlob("photo");
                in = blob.getBinaryStream();
                BufferedImage image = ImageIO.read(in);
                
                OutputStream os = new FileOutputStream("E:\\image1.png");

        		byte[] bb = new byte[2048];
        		int length;

        		while ((length = in.read(bb)) != -1) {
        			os.write(bb, 0, length);
        		}

        		in.close();
        		os.close();
//                ImageIO.write(image, "png",file);
                //return ", "+in +" , " + image+ "RETURN --> " + ImageIO.write(image, "png",file);
//                b=blob.getBytes(1,(int)blob.length());
//                fos.write(b);
            }
//            ps.close();
//            fos.close();
//            con.close();
        }catch(Exception e){
            e.printStackTrace();
        }
		return null;
	}
	
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	public int getassigned_not(){
		return this.assigned_not;
	}
	public void setassigned_not(int s){
		this.assigned_not = s;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the carNumber
	 */
	public String getCarNumber() {
		return carNumber;
	}
	/**
	 * @param carNumber the carNumber to set
	 */
	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}
	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}
	/**
	 * @param model the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}
	/**
	 * @return the style
	 */
	public String getStyle() {
		return style;
	}
	/**
	 * @param style the style to set
	 */
	public void setStyle(String style) {
		this.style = style;
	}
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}
	/**
	 * @return the carDriverNationalId
	 */
	public int getCarDriverNationalId() {
		return carDriverNationalId;
	}
	/**
	 * @param carDriverNationalId the carDriverNationalId to set
	 */
	public void setCarDriverNationalId(int carDriverNationalId) {
		this.carDriverNationalId = carDriverNationalId;
	}
	/**
	 * @return the countKm
	 */
	public double getCountKm() {
		return countKm;
	}
	/**
	 * @param countKm the countKm to set
	 */
	public void setCountKm(double countKm) {
		this.countKm = countKm;
	}
	/**
	 * @return the monthlyInstallment
	 */
	public double getMonthlyInstallment() {
		return monthlyInstallment;
	}
	/**
	 * @param monthlyInstallment the monthlyInstallment to set
	 */
	public void setMonthlyInstallment(double monthlyInstallment) {
		this.monthlyInstallment = monthlyInstallment;
	}
	/**
	 * @return the totalLoan
	 */
	public double getTotalLoan() {
		return totalLoan;
	}
	/**
	 * @param totalLoan the totalLoan to set
	 */
	public void setTotalLoan(double totalLoan) {
		this.totalLoan = totalLoan;
	}
	/**
	 * @return the remainLoan
	 */
	public double getRemainLoan() {
		return remainLoan;
	}
	/**
	 * @param remainLoan the remainLoan to set
	 */
	public void setRemainLoan(double remainLoan) {
		this.remainLoan = remainLoan;
	}
	/**
	 * @return the firstInstallmentDate
	 */
	public String getFirstInstallmentDate() {
		return firstInstallmentDate;
	}
	/**
	 * @param firstInstallmentDate the firstInstallmentDate to set
	 */
	public void setFirstInstallmentDate(String firstInstallmentDate) {
		this.firstInstallmentDate = firstInstallmentDate;
	}
	/**
	 * @return the carStatus
	 */
	public String getCarStatus() {
		return carStatus;
	}
	/**
	 * @param carStatus the carStatus to set
	 */
	public void setCarStatus(String carStatus) {
		this.carStatus = carStatus;
	}
	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}
	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	/**
	 * @return the carCompanyName
	 */
	public String getCarCompanyName() {
		return carCompanyName;
	}
	/**
	 * @param carCompanyName the carCompanyName to set
	 */
	public void setCarCompanyName(String carCompanyName) {
		this.carCompanyName = carCompanyName;
	}
	
	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}
	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	/**
	 * @return the totalFines
	 */
	public double getTotalFines() {
		return totalFines;
	}
	/**
	 * @param totalFines the totalFines to set
	 */
	public void setTotalFines(double totalFines) {
		this.totalFines = totalFines;
	}
	
	public void insertImagePath(String path) throws SQLException{
		conn=DBconnection.getActiveConnection();
		String sql="insert into test values(?)";
		PreparedStatement stmt=conn.prepareStatement(sql);
		stmt.setString(1, path);
		stmt.executeUpdate();
		
	}
	
	public String getImagePath() throws SQLException{
		conn=DBconnection.getActiveConnection();
		String sql="select * from contacts";
		PreparedStatement stmt=conn.prepareStatement(sql);
		ResultSet rs=stmt.executeQuery();
		while(rs.next())
			return rs.getString("photo");
		
		return null;
	}

	public int addNewCar(String carNumber,String type,String model,String style,String color,
			int carDriverNationalId,double countKm,double monthlyInstallment,double totalLoan,
			double remainLoan,String firstInstallmentDate,String carStatus,String paymentType,
			String carCompanyName,/*String imageUrl,*/double totalFines,int assigned_not) throws SQLException{
			
		conn=DBconnection.getActiveConnection();
		
		if(carReport(carNumber)!=null)
			return 0;
		
		/*EmployeeModel emp = new EmployeeModel();
		if(emp.employeeReport(carDriverNationalId) ==null)
			return 0;
		*/
		/*FinanceCompanyModel fobj = new FinanceCompanyModel();
		if(fobj.getFinanceCompany(carCompanyName) == null)
			return 0;
		*/
		String sql="insert into car(carNumber,type_,model,style,color,carDriverNationalId,countKm,monthlyInstallment,totalLoan,remainLoan,firstInstallmentDate,carStatus,paymentType,carCompanyName,totalFines,assigned_not) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setString(1,carNumber);  
		stmt.setString(2,type);
		stmt.setString(3,model);
		stmt.setString(4,style);
		stmt.setString(5,color);
		stmt.setInt(6,carDriverNationalId);
		stmt.setDouble(7,countKm);
		stmt.setDouble(8,monthlyInstallment);
		stmt.setDouble(9,totalLoan);
		stmt.setDouble(10,remainLoan);
		stmt.setString(11,firstInstallmentDate);
		stmt.setString(12,carStatus);
		stmt.setString(13,paymentType);
		stmt.setString(14,carCompanyName);
		//stmt.setString(15,imageUrl);
		stmt.setDouble(15,totalFines);
		stmt.setInt(16, assigned_not); 
		
		return stmt.executeUpdate();
	}
	
	public CarModel carReport(String carNumber) throws SQLException{
			
		conn=DBconnection.getActiveConnection();
		String sql="select * from car where carNumber=?";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setString(1,carNumber);
		
		
		ResultSet rs=stmt.executeQuery();
		
		CarModel obj=new CarModel();
		
		while (rs.next()){
			obj.setCarNumber(rs.getString("carNumber")); /**2**/
			obj.setCarDriverNationalId(rs.getInt("carDriverNationalId")); /**2**/
			obj.setCountKm(rs.getDouble("countKm")); /**2**/
			
			obj.setCarCompanyName(rs.getString("carCompanyName")); /**1**/
			obj.setModel(rs.getString("model"));  /**1**/
			
			obj.setType(rs.getString("type_"));
			obj.setStyle(rs.getString("style"));
			obj.setCarStatus(rs.getString("carStatus"));
			obj.setTotalFines(rs.getDouble("totalFines"));
			
			obj.setColor(rs.getString("color")); /**image**/
			obj.setImageUrl(rs.getString("image")); /**image**/

			obj.setMonthlyInstallment(rs.getDouble("monthlyInstallment"));
			obj.setFirstInstallmentDate(rs.getDate("firstInstallmentDate").toString());
			
			obj.setTotalLoan(rs.getDouble("totalLoan"));
			obj.setRemainLoan(rs.getDouble("remainLoan"));
			obj.setPaymentType(rs.getString("paymentType"));
			
			obj.setassigned_not(rs.getInt("assigned_not"));
			
			return obj;
		}
		
		return null;
	}
	
	public CarModel carReportByDriver(int NationalId) throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="select * from car where carNumber=?)";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setInt(1,NationalId);
		
		
		ResultSet rs=stmt.executeQuery();
		
		CarModel obj=new CarModel();
		
		while (rs.next()){
			obj.setCarNumber(rs.getString("carNumber"));
			obj.setType(rs.getString("type_"));
			obj.setModel(rs.getString("model"));
			obj.setStyle(rs.getString("style"));
			obj.setColor(rs.getString("color"));
			obj.setCarDriverNationalId(rs.getInt("carDriverNationalId"));
			obj.setCountKm(rs.getDouble("countKm"));
			obj.setMonthlyInstallment(rs.getDouble("monthlyInstallment"));
			obj.setTotalLoan(rs.getDouble("totalLoan"));
			obj.setRemainLoan(rs.getDouble("remainLoan"));
			obj.setFirstInstallmentDate(rs.getDate("firsrInstallmentDate").toString());
			obj.setCarStatus(rs.getString("carStatus"));
			obj.setPaymentType(rs.getString("paymentType"));
			obj.setCarCompanyName(rs.getString("carCompanyName"));
			obj.setImageUrl(rs.getString("image"));
			obj.setTotalFines(rs.getDouble("totalFines"));
			
			return obj;
		}
		
		return null;
	}
public int  updateCountKm(String carNumber,double countKm) throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="update car set countKm=? where carNumber= ?)";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setDouble(1,countKm);
		stmt.setString(2,carNumber)	;	
		
		return stmt.executeUpdate();
	}

public int  UpdateCarDriver(String carNumber,int nationalId) throws SQLException{
	
	EmployeeModel emp = new EmployeeModel();
	if(emp.employeeReport(nationalId)==null)
		return -1;
	
	//check driver doesn't assigned to a car already! assigned_not
	if(!emp.checkDriverAssign(nationalId))
		return -1;
	
	conn=DBconnection.getActiveConnection();
	String sql="select * from car where carNumber= ? and assigned_not=?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setString(1, carNumber);
	stmt.setInt(2,1);
	ResultSet rs =  stmt.executeQuery();
	while(rs.next())
		return -1;
	
	conn=DBconnection.getActiveConnection();
	sql="update car set carDriverNationalId=?,assigned_not=? where carNumber= ?";
	stmt=conn.prepareStatement(sql);
	
	stmt.setInt(1,nationalId);
	stmt.setInt(2, 1);
	stmt.setString(3,carNumber)	;
	
	stmt.executeUpdate();
	
	sql="update employee set assigned_not=? where nationalId= ?";
	stmt=conn.prepareStatement(sql);
	
	stmt.setInt(1,1);
	stmt.setInt(2,nationalId);
	
	return stmt.executeUpdate();
	}
public int  updateRemainLoan(String carNumber,double amount) throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="update car set amount=? where carNumber= ?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setDouble(1,amount);
	stmt.setString(2,carNumber)	;	
	
	return stmt.executeUpdate();
}
public int  updateCarStatus(String carNumber,String status) throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="update car set carStatus=? where carNumber= ?)";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setString(1,status);
	stmt.setString(2,carNumber)	;	
	
	return stmt.executeUpdate();
}
//-------

public int  updateCar(String carNumber,/*int carDriverNationalId,*/double countKm,double remainLoan,
		String status,String paymentType/*,String imageUrl*/) throws SQLException{
	
	if(carReport(carNumber)==null)
		return -1;
	conn=DBconnection.getActiveConnection();
	String sql="update car set carStatus=?,countKm=?,remainLoan=?,paymentType=? where carNumber= ?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setString(1,status);
	//stmt.setInt(2,carDriverNationalId)	;
	stmt.setDouble(2,countKm);
	stmt.setDouble(3,remainLoan)	;
	stmt.setString(4,paymentType);
	//stmt.setString(6,imageUrl)	;
	stmt.setString(5,carNumber);
	
	return stmt.executeUpdate();
}
//---------
/*public int  updateCarMaintainanceCompany(String carNumber,String CompanyName) throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="update car set maintainanceComponyName=? where carNumber= ?)";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setString(1,CompanyName);
	stmt.setString(2,carNumber)	;	
	
	return stmt.executeUpdate();
	}*/
public int  updateTotalFines(String carNumber,double amount) throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="update car set TotalFines=? where carNumber= ?)";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setDouble(1,amount);
	stmt.setString(2,carNumber)	;	
	
	return stmt.executeUpdate();
	}

public int checkDriverAssign(String carNumber) throws SQLException{
	conn=DBconnection.getActiveConnection();
	String sql="select * from car where carNumber= ? and assigned_not=?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setString(1,carNumber);
	stmt.setInt(2,1);
	ResultSet rs = stmt.executeQuery();
	while(rs.next()){
		return rs.getInt("carDriverNationalId"); //not assigned
	}
	 
	return -1;
}
public int un_assignEmployee(String carNumber) throws SQLException{
	if(checkDriverAssign(carNumber)==-1) //not assigned
		return -1;
	
	conn=DBconnection.getActiveConnection();
	String sql="update employee set assigned_not=? where nationalId= ?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setInt(1,0);
	stmt.setInt(2,checkDriverAssign(carNumber));	
	
	return stmt.executeUpdate();
}
public int  deleteCar(String carNumber) throws SQLException{
	
	//check if its assigned and modify its driver
	un_assignEmployee(carNumber);
	
	conn=DBconnection.getActiveConnection();
	/*if(carReport(carNumber)==null)
		return -1;*/
	String sql="delete from car where carNumber= ?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setString(1,carNumber)	;	
	
	return stmt.executeUpdate();
	}


public int un_assignDriver_Car(String carNumber) throws SQLException{
	
	int driverNationalID = checkDriverAssign(carNumber);
	if(driverNationalID==-1) //not assigned
		return -1;
	
	conn=DBconnection.getActiveConnection();
	String sql="update employee set assigned_not=? where nationalId= ?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setInt(1,0);
	stmt.setInt(2,driverNationalID);	
	
	stmt.executeUpdate();
	
	sql="update car set assigned_not=? and carDriverNationalId=? where carNumber= ?";
	stmt=conn.prepareStatement(sql);
	
	stmt.setInt(1,0);
	stmt.setInt(2, 0);
	stmt.setString(3,carNumber);
	
	return stmt.executeUpdate();
}


public ArrayList<CarModel> getAllCars() throws SQLException{
	ArrayList<CarModel> array = new ArrayList<CarModel>();
	
	conn=DBconnection.getActiveConnection();
	String sql="select * from car";
	PreparedStatement stmt=conn.prepareStatement(sql);
	ResultSet rs=stmt.executeQuery();
	
	while (rs.next()){
		CarModel obj = new CarModel();
		 
		obj.setCarNumber(rs.getString("carNumber"));
		obj.setType(rs.getString("type_"));
		obj.setModel(rs.getString("model"));
		obj.setStyle(rs.getString("style"));
		obj.setColor(rs.getString("color"));
		obj.setCarDriverNationalId(rs.getInt("carDriverNationalId"));
		obj.setCountKm(rs.getDouble("countKm"));
		obj.setMonthlyInstallment(rs.getDouble("monthlyInstallment"));
		obj.setTotalLoan(rs.getDouble("totalLoan"));
		obj.setRemainLoan(rs.getDouble("remainLoan"));
		obj.setFirstInstallmentDate(rs.getDate("firstInstallmentDate").toString());
		obj.setCarStatus(rs.getString("carStatus"));
		obj.setPaymentType(rs.getString("paymentType"));
		obj.setCarCompanyName(rs.getString("carCompanyName"));
		obj.setImageUrl(rs.getString("image"));
		obj.setTotalFines(rs.getDouble("totalFines"));
		obj.setassigned_not(rs.getInt("assigned_not"));
		
		array.add(obj);
	}
	return array;
}

}
