package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MaintainanceModel {
	
	private String companyName;
	private String carNumber;
	private String serviceType;
	private double serviceCost;
	private String serviceDate;
	private String nextServiceDate;
	private Connection conn=null;
	
	public MaintainanceModel(){}


	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}


	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	/**
	 * @return the carNumber
	 */
	public String getCarNumber() {
		return carNumber;
	}


	/**
	 * @param carnumber the carNumber to set
	 */
	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}


	/**
	 * @return the serviceType
	 */
	public String getServiceType() {
		return serviceType;
	}


	/**
	 * @param serviceType the serviceType to set
	 */
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}


	/**
	 * @return the serviceCost
	 */
	public double getServiceCost() {
		return serviceCost;
	}


	/**
	 * @param serviceCost the serviceCost to set
	 */
	public void setServiceCost(double serviceCost) {
		this.serviceCost = serviceCost;
	}


	/**
	 * @return the serviceDate
	 */
	public String getServiceDate() {
		return serviceDate;
	}


	/**
	 * @param serviceDate the serviceDate to set
	 */
	public void setServiceDate(String serviceDate) {
		this.serviceDate = serviceDate;
	}


	/**
	 * @return the nextServiceDate
	 */
	public String getNextServiceDate() {
		return nextServiceDate;
	}


	/**
	 * @param nextServiceDate the nextServiceDate to set
	 */
	public void setNextServiceDate(String nextServiceDate) {
		this.nextServiceDate = nextServiceDate;
	}
	
	public int addNewMaintainance(String companyName,String carNumber,String serviceType,double serviceCost,
			String serviceDate,String nextServiceDate)throws SQLException{
			
		conn=DBconnection.getActiveConnection();
		String sql="insert into maintainance values(?,?,?,?,?,?)";
		PreparedStatement stmt=conn.prepareStatement(sql);
		CarModel car=new CarModel();
		if(car.carReport(carNumber)==null){
			return 0;
		}
		stmt.setString(1,companyName);
		stmt.setString(2,carNumber);
		stmt.setString(3,serviceType);
		stmt.setDouble(4,serviceCost);
		stmt.setString(5,serviceDate);
		stmt.setString(6,nextServiceDate);
		
		return stmt.executeUpdate();
		
		
	}
	
	public int addNewMaintainance(MaintainanceModel obj)throws SQLException{
			
		conn=DBconnection.getActiveConnection();
		String sql="insert into maintainance values(?,?,?,?,?,?)";
		PreparedStatement stmt=conn.prepareStatement(sql);
		CarModel car=new CarModel();
		if(car.carReport(obj.getCarNumber())==null){
			return 0;
		}
		stmt.setString(1,obj.getCompanyName());
		stmt.setString(2,obj.getCarNumber());
		stmt.setString(3,obj.getServiceType());
		stmt.setDouble(4,obj.getServiceCost());
		stmt.setString(5,obj.getServiceDate());
		stmt.setString(6,obj.getNextServiceDate());
		
		return stmt.executeUpdate();
		
	}
	
	//-------------------------------
	public void updateCarMaintaine(MaintainanceModel obj) throws SQLException{

		
		conn=DBconnection.getActiveConnection();
		String sql = "update maintainance set companyName=?,serviceType=?,serviceCost=?,"
				+ "serviceDate=?,nextServiceDate=? where carNumber=? and companyName=?";
		
		PreparedStatement stmt = conn.prepareStatement(sql);
		
		stmt.setString(1,obj.getCompanyName());
		stmt.setString(2,obj.getServiceType());
		stmt.setDouble(3,obj.getServiceCost());
		stmt.setString(4,obj.getServiceDate());
		stmt.setString(5,obj.getNextServiceDate());
		stmt.setString(6,obj.getCarNumber());
		stmt.setString(7,obj.getCompanyName());
		
		stmt.executeUpdate();
	}
	//-------------------------------
	
	public double totalCostForPeroid(String carNumber,String fromDate,String toDate)throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="select sum(serviceCost) as serviceTotalCost from maintainance where carNumber=? and serviceDate> ? and serviceDate<?";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setString(1,carNumber);
		stmt.setString(2,fromDate);
		stmt.setString(3,toDate);
		
		ResultSet rs=stmt.executeQuery();
		double amount=-1;
		
		while (rs.next()){
			amount=rs.getDouble("serviceTotalCost");
			
		}
		
		return amount;
	}
	
public double ServiceCost(String carNumber,String date)throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="select sum(ServiceCost) as total from maintainance where carNumber=? and serviceDate=?";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setString(1,carNumber);
		stmt.setString(2,date);
		
		ResultSet rs=stmt.executeQuery();
		double amount=-1;
		
		while (rs.next()){
			amount=rs.getDouble("total");
		}	
		return amount;
	}
public ArrayList<MaintainanceModel> maintainanceReport(String carNumber)throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="select * from maintainance where carNumber=?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setString(1,carNumber);
	
	ResultSet rs=stmt.executeQuery();
	ArrayList<MaintainanceModel>array=new ArrayList<MaintainanceModel>();
	MaintainanceModel obj=new MaintainanceModel();
	
	while (rs.next()){
		obj.setCompanyName(rs.getString("companyName"));
		obj.setCarNumber(rs.getString("carNumber"));
		obj.setServiceType(rs.getString("serviceType"));
		obj.setServiceCost(rs.getDouble("serviceCost"));
		obj.setServiceDate(rs.getDate("serviceDate").toString());
		obj.setNextServiceDate(rs.getDate("nextServiceDate").toString());
		
		array.add(obj);
	}
	
	return array;
}


public ArrayList<MaintainanceModel> getAllCarsMaintain()throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="select * from maintainance";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	ResultSet rs=stmt.executeQuery();
	ArrayList<MaintainanceModel>array=new ArrayList<MaintainanceModel>();
	
	
	while (rs.next()){
		MaintainanceModel obj=new MaintainanceModel();
		
		obj.setCompanyName(rs.getString("companyName"));
		obj.setCarNumber(rs.getString("carNumber"));
		obj.setServiceType(rs.getString("serviceType"));
		obj.setServiceCost(rs.getDouble("serviceCost"));
		obj.setServiceDate(rs.getDate("serviceDate").toString());
		obj.setNextServiceDate(rs.getDate("nextServiceDate").toString());
		
		array.add(obj);
	}
	
	return array;
}



public void  deleteMaintain(String carNumber) throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="delete from maintainance where carNumber= ?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setString(1,carNumber);	
	
	stmt.executeUpdate();
	}

//-------------------------------
@Override
public String toString() {

	return "MaintainanceModel [companyName=" + companyName + ", carNumber=" + carNumber
			+ ", serviceType=" + serviceType + ", serviceCost="+ serviceCost + ", serviceDate=" +serviceDate + ", nextServiceDate=" +nextServiceDate +"]";
}
//-------------------------------


}
