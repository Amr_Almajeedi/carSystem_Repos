package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class FineModel {

	private String fineType;
	private String description;
	private String carNumber;
	private int driverNationalId;
	private double fineAmount;
	private String fineDate;
	private Connection conn=null;
	public FineModel(){
		
	}

	/**
	 * @return the fineType
	 */
	public String getFineType() {
		return fineType;
	}

	/**
	 * @param fineType the fineType to set
	 */
	public void setFineType(String fineType) {
		this.fineType = fineType;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the carNumber
	 */
	public String getCarNumber() {
		return carNumber;
	}

	/**
	 * @param carNumber the carNumber to set
	 */
	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	/**
	 * @return the driverNationalId
	 */
	public int getDriverNationalId() {
		return driverNationalId;
	}

	/**
	 * @param driverNationalId the driverNationalId to set
	 */
	public void setDriverNationalId(int driverNationalId) {
		this.driverNationalId = driverNationalId;
	}

	/**
	 * @return the fineAmount
	 */
	public double getFineAmount() {
		return fineAmount;
	}

	/**
	 * @param fineAmount the fineAmount to set
	 */
	public void setFineAmount(double fineAmount) {
		this.fineAmount = fineAmount;
	}

	/**
	 * @return the fineDate
	 */
	public String getFineDate() {
		return fineDate;
	}

	/**
	 * @param fineDate the fineDate to set
	 */
	public void setFineDate(String fineDate) {
		this.fineDate = fineDate;
	}
	
	public int addNewFine(String fineType,String description,String carNumber,int driverNationalId,
			double fineAmount,String fineDate) throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="insert into fine(fineType,description,carNumber,driverNationalId,fineAmount,fineDate) values(?,?,?,?,?,?)";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setString(1,fineType);
		stmt.setString(2,description);
		stmt.setString(3,carNumber);
		stmt.setInt(4,driverNationalId);
		stmt.setDouble(5,fineAmount);
		stmt.setString(6,fineDate);
		
		return stmt.executeUpdate();
	}
	
public ArrayList<FineModel> fineReport(String carNumber)throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="select * from fine where carNumber=?)";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setString(1,carNumber);
		
		ResultSet rs=stmt.executeQuery();
		ArrayList<FineModel>array=new ArrayList<FineModel>();
		FineModel obj=new FineModel();
		
		while (rs.next()){
			obj.setFineType(rs.getString("fineType"));
			obj.setDescription(rs.getString("description"));
			obj.setCarNumber(rs.getString("carNumber"));
			obj.setDriverNationalId(rs.getInt("driverNationalId"));
			obj.setFineAmount(rs.getDouble("fineAmount"));
			obj.setFineDate(rs.getDate("fineDate").toString());
			
			array.add(obj);
		}
		
		return array;
	}

public ArrayList<FineModel> fineReportByDriver(int nationalId)throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="select * from fine where nationalId=?)";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setInt(1,nationalId);
	
	ResultSet rs=stmt.executeQuery();
	ArrayList<FineModel>array=new ArrayList<FineModel>();
	FineModel obj=new FineModel();
	
	while (rs.next()){
		obj.setFineType(rs.getString("fineType"));
		obj.setDescription(rs.getString("description"));
		obj.setCarNumber(rs.getString("carNumber"));
		obj.setDriverNationalId(rs.getInt("driverNationalId"));
		obj.setFineAmount(rs.getDouble("fineAmount"));
		obj.setFineDate(rs.getDate("fineDate").toString());
		
		array.add(obj);
	}
	
	return array;
}

public double totalFines(String carNumber)throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="select sum(fineAmount) from fine where carNumber=?)";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setString(1,carNumber);
	
	ResultSet rs=stmt.executeQuery();
	double amount=-1;
	
	while (rs.next()){
		amount=rs.getDouble("fineAmount");
	}
	
	return amount;
}

public double employeeTotalFines(int nationalId)throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="select sum(fineAmount) from fine where driverNationalId=?)";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setInt(1,nationalId);
	
	ResultSet rs=stmt.executeQuery();
	double amount=-1;
	
	while (rs.next()){
		amount=rs.getDouble("fineAmount");
		
	}
	
	return amount;
}

public double dayTotalFines(String date)throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="select sum(fineAmount) from fine where date=?)";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setString(1,date);
	
	ResultSet rs=stmt.executeQuery();
	double amount=-1;
	
	while (rs.next()){
		amount=rs.getDouble("fineAmount");
		
	}
	
	return amount;
}

public double totalFinesForPeroid(String carNumber,String fromDate,String toDate)throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="select sum(fineAmount) from fine where carNumber=? and fineDate between(?,?)";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setString(1,carNumber);
	stmt.setString(2,fromDate);
	stmt.setString(3,toDate);
	
	ResultSet rs=stmt.executeQuery();
	double amount=-1;
	
	while (rs.next()){
		amount=rs.getDouble("fineAmount");
		
	}
	
	return amount;
}



}
