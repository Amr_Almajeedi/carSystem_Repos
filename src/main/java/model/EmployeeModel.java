package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class EmployeeModel {

	private int nationalId;
	private int employeeNumber;
	private String name;
	private String nationality;
	private String gender;
	private String birthDate;
	private String mobilePhone;
	private double salary;
	private String jobTitle;
	private String hiringDate;
	private String passportNumber;
	private String maritalStatus;
	private String sponsorName;
	private String drivingLicenceNumber;
	private String drivingExpireDate;
	private double houseAllowence;
	private String careamId;
	private String uberId;
	private String password;
	private int assigned_not;
	private String status_assigned_not;
	
	public void setstatus_assigned_not(String s){
		this.status_assigned_not = s;
		
	}
	public String getstatus_assigned_not(){
		return this.status_assigned_not;
	}
	
	private Connection conn=null;
	
	public EmployeeModel(){}
	/**
	 * @return the nationalId
	 */
	public int getNationalId() {
		return nationalId;
	}
	
	public void setassigned_not(int s){
		this.assigned_not=s;
	}
	public int getassigned_not(){
		return this.assigned_not;
	}
	/**
	 * @param nationalId the nationalId to set
	 */
	public void setNationalId(int nationalId) {
		this.nationalId = nationalId;
	}
	/**
	 * @return the employeeNumber
	 */
	public int getEmployeeNumber() {
		return employeeNumber;
	}
	/**
	 * @param employeeNumber the employeeNumber to set
	 */
	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}
	/**
	 * @param nationality the nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the birthDate
	 */
	public String getBirthDate() {
		return birthDate;
	}
	/**
	 * @param birthDate the birthDate to set
	 */
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}
	/**
	 * @param mobilePhone the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	/**
	 * @return the salary
	 */
	public double getSalary() {
		return salary;
	}
	/**
	 * @param salary the salary to set
	 */
	public void setSalary(double salary) {
		this.salary = salary;
	}
	/**
	 * @return the jobTitle
	 */
	public String getJobTitle() {
		return jobTitle;
	}
	/**
	 * @param jobTitle the jobTitle to set
	 */
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	/**
	 * @return the hiringDate
	 */
	public String getHiringDate() {
		return hiringDate;
	}
	/**
	 * @param hiringDate the hiringDate to set
	 */
	public void setHiringDate(String hiringDate) {
		this.hiringDate = hiringDate;
	}
	/**
	 * @return the passportNumber
	 */
	public String getPassportNumber() {
		return passportNumber;
	}
	/**
	 * @param passportNumber the passportNumber to set
	 */
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	/**
	 * @return the maritalStatus
	 */
	public String getMaritalStatus() {
		return maritalStatus;
	}
	/**
	 * @param maritalStatus the maritalStatus to set
	 */
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	/**
	 * @return the sponsorName
	 */
	public String getSponsorName() {
		return sponsorName;
	}
	/**
	 * @param sponsorName the sponsorName to set
	 */
	public void setSponsorName(String sponsorName) {
		this.sponsorName = sponsorName;
	}
	/**
	 * @return the drivingLicenceNumber
	 */
	public String getDrivingLicenceNumber() {
		return drivingLicenceNumber;
	}
	/**
	 * @param drivingLicenceNumber the drivingLicenceNumber to set
	 */
	public void setDrivingLicenceNumber(String drivingLicenceNumber) {
		this.drivingLicenceNumber = drivingLicenceNumber;
	}
	/**
	 * @return the drivingExpireDate
	 */
	public String getDrivingExpireDate() {
		return drivingExpireDate;
	}
	/**
	 * @param drivingExpireDate the drivingExpireDate to set
	 */
	public void setDrivingExpireDate(String drivingExpireDate) {
		this.drivingExpireDate = drivingExpireDate;
	}
	/**
	 * @return the houseAllowence
	 */
	public double getHouseAllowence() {
		return houseAllowence;
	}
	/**
	 * @param houseAllowence the houseAllowence to set
	 */
	public void setHouseAllowence(double houseAllowence) {
		this.houseAllowence = houseAllowence;
	}
	/**
	 * @return the careamId
	 */
	public String getCareamId() {
		return careamId;
	}
	/**
	 * @param careamId the careamId to set
	 */
	public void setCareamId(String careamId) {
		this.careamId = careamId;
	}
	/**
	 * @return the uberId
	 */
	public String getUberId() {
		return uberId;
	}
	/**
	 * @param uberId the uberId to set
	 */
	public void setUberId(String uberId) {
		this.uberId = uberId;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	public int addNewEmployee(int nationalId,int employeeNumber,String name,String nationality,String gender,
			String birthDate,String mobilePhone,double salary,String jobTitle,String hiringDate,
			String passportNumber,String maritalStatus,String sponsorName,String drivingLicenceNumber,
			String drivingExpireDate,double houseAllowence,String careamId,String uberId,String password) throws SQLException{
			
		conn=DBconnection.getActiveConnection();
		String sql="insert into employee(nationalId,employeeNumber,name_,nationality,gender,birthDate,"
				+ "mobilePhone,salary,jobTitle,hiringDate,passportNumber,maritalStatus,sponsorName,"
				+ "drivingLicenceNumber,drivingExpireDate,houseAllowence,careamId,uberId,password_) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setInt(1,nationalId);
		stmt.setInt(2,employeeNumber);
		stmt.setString(3,name);
		stmt.setString(4,nationality);
		stmt.setString(5,gender);
		stmt.setString(6,birthDate);
		stmt.setString(7,mobilePhone);
		stmt.setDouble(8,salary);
		stmt.setString(9,jobTitle);
		stmt.setString(10,hiringDate);
		stmt.setString(11,passportNumber);
		stmt.setString(12,maritalStatus);
		stmt.setString(13,sponsorName);
		stmt.setString(14,drivingLicenceNumber);
		stmt.setString(15,drivingExpireDate);
		stmt.setDouble(16,houseAllowence);
		stmt.setString(17,careamId);
		stmt.setString(18,uberId);
		stmt.setString(19,password);
		
		return stmt.executeUpdate();
	}
	
	public EmployeeModel employeeReport(int nationalId) throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="select * from employee where nationalId=?";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setInt(1,nationalId);
		
		ResultSet rs=stmt.executeQuery();
		EmployeeModel obj=new EmployeeModel();
		
		while (rs.next()){
			obj.setNationalId(rs.getInt("nationalId"));
			obj.setEmployeeNumber(rs.getInt("employeeNumber"));
			obj.setName(rs.getString("name_"));
			obj.setNationality(rs.getString("nationality"));
			obj.setGender(rs.getString("gender"));
			obj.setBirthDate(rs.getDate("birthDate").toString());
			obj.setMobilePhone(rs.getString("mobilePhone"));
			obj.setSalary(rs.getDouble("salary"));
			obj.setJobTitle(rs.getString("jobTitle"));
			obj.setHiringDate(rs.getDate("hiringDate").toString());
			obj.setPassportNumber(rs.getString("passportNumber"));
			obj.setMaritalStatus(rs.getString("maritalStatus"));
			obj.setSponsorName(rs.getString("sponsorName"));
			obj.setDrivingLicenceNumber(rs.getString("drivingLicenceNumber"));
			obj.setDrivingExpireDate(rs.getDate("drivingExpireDate").toString());
			obj.setHouseAllowence(rs.getDouble("houseAllowence"));
			obj.setCareamId(rs.getString("careamId"));
			obj.setUberId(rs.getString("uberId"));
			obj.setPassword(rs.getString("password_"));
			return obj;
		}
		return null;	
	}
	
public EmployeeModel logIn(int nationalId,String password) throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="select * from employee where nationalId=? and password_=?";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setInt(1,nationalId);
		stmt.setString(2,password);
		ResultSet rs=stmt.executeQuery();
		
		EmployeeModel obj=new EmployeeModel();
		
		while (rs.next()){
			obj.setNationalId(rs.getInt("nationalId"));
			obj.setEmployeeNumber(rs.getInt("employeeNumber"));
			obj.setName(rs.getString("name_"));
			obj.setNationality(rs.getString("nationality"));
			obj.setGender(rs.getString("gender"));
			obj.setBirthDate(rs.getDate("birthDate").toString());
			obj.setMobilePhone(rs.getString("mobilePhone"));
			obj.setSalary(rs.getDouble("salary"));
			obj.setJobTitle(rs.getString("jobTitle"));
			obj.setHiringDate(rs.getDate("hiringDate").toString());
			obj.setPassportNumber(rs.getString("passportNumber"));
			obj.setMaritalStatus(rs.getString("maritalStatus"));
			obj.setSponsorName(rs.getString("sponsorName"));
			obj.setDrivingLicenceNumber(rs.getString("drivingLicenceNumber"));
			obj.setDrivingExpireDate(rs.getDate("drivingExpireDate").toString());
			obj.setHouseAllowence(rs.getDouble("houseAllowence"));
			obj.setCareamId(rs.getString("careamId"));
			obj.setUberId(rs.getString("uberId"));
			obj.setPassword(rs.getString("password_"));
			return obj;
		}
		return null;
	}
public int  updateSalary(int nationalId,double salary) throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="update employee set salary=? where nationalId= ?";
		PreparedStatement stmt=conn.prepareStatement(sql);
		
		stmt.setDouble(1,salary);
		stmt.setInt(2,nationalId);	
		
		return stmt.executeUpdate();
	}
public int  updateJob(int nationalId,String job) throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="update employee set jobTitle=? where nationalId= ?)";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setString(1,job);
	stmt.setInt(2,nationalId);	
	
	return stmt.executeUpdate();
}

public int  updateMaritalStatus(int nationalId,String status) throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="update employee set maritalStatus=? where nationalId= ?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setString(1,status);
	stmt.setInt(2,nationalId);	
	
	return stmt.executeUpdate();
}
public int  updateSponsorName(int nationalId,String name) throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="update employee set sponsorName=? where nationalId= ?)";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setString(1,name);
	stmt.setInt(2,nationalId);	
	
	return stmt.executeUpdate();
}
public int  updateHouseAllownce(int nationalId,double amount) throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="update employee set houseAllownce=? where nationalId= ?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setDouble(1,amount);
	stmt.setInt(2,nationalId);	
	
	return stmt.executeUpdate();
}
public int  updatePassword(int nationalId,String pass) throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="update employee set password_=? where nationalId= ?)";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setString(1,pass);
	stmt.setInt(2,nationalId);	
	
	return stmt.executeUpdate();
}
public int  updateMobile(int nationalId,String number) throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="update employee set mobilePhone=? where nationalId= ?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setString(1,number);
	stmt.setInt(2,nationalId);	
	
	return stmt.executeUpdate();
}
public ArrayList<EmployeeModel> getEmployeeByJob(String job) throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="select * from employee where jobTitle=?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setString(1,job);
	
	ResultSet rs=stmt.executeQuery();
	ArrayList<EmployeeModel> array=new ArrayList<EmployeeModel>();
	EmployeeModel obj=new EmployeeModel();
	
	while (rs.next()){
		obj.setNationalId(rs.getInt("nationalId"));
		obj.setEmployeeNumber(rs.getInt("employeeNumber"));
		obj.setName(rs.getString("name_"));
		obj.setNationality(rs.getString("nationality"));
		obj.setGender(rs.getString("gender"));
		obj.setBirthDate(rs.getDate("birthDate").toString());
		obj.setMobilePhone(rs.getString("mobilePhone"));
		obj.setSalary(rs.getDouble("salary"));
		obj.setJobTitle(rs.getString("jobTitle"));
		obj.setHiringDate(rs.getDate("hiringDate").toString());
		obj.setPassportNumber(rs.getString("passportNumber"));
		obj.setMaritalStatus(rs.getString("maritalStatus"));
		obj.setSponsorName(rs.getString("sponsorName"));
		obj.setDrivingLicenceNumber(rs.getString("drivingLicenceNumber"));
		obj.setDrivingExpireDate(rs.getDate("drivingExpireDate").toString());
		obj.setHouseAllowence(rs.getDouble("houseAllowence"));
		obj.setCareamId(rs.getString("careamId"));
		obj.setUberId(rs.getString("uberId"));
		obj.setPassword(rs.getString("password_"));
		
		array.add(obj);
	}
	
	return array;
}

public int getEmployeeCount(){
	int count = 0;
	conn=DBconnection.getActiveConnection();
	
	try {
		Statement stm =conn.createStatement();
		ResultSet rs = stm.executeQuery("select count(*) as count from employee");
		while(rs.next())
			return count;
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return count;
}

public ArrayList<EmployeeModel> getAllEmployee_II(/*int jtStartIndex, int jtPageSize*/) throws SQLException{
	ArrayList<EmployeeModel> array = new ArrayList<EmployeeModel>();
	
	/*String startIndex = Integer.toString(jtStartIndex);
	String pageSize = Integer.toString(jtPageSize);
	*/
	conn=DBconnection.getActiveConnection();
	//limit "+startIndex+","+pageSize
	String sql="select * from employee where assigned_not!=?" ;
	PreparedStatement stmt=conn.prepareStatement(sql);
	stmt.setInt(1, -1);
	
	ResultSet rs=stmt.executeQuery();
	
	while(rs.next()){
		EmployeeModel obj=new EmployeeModel();
		obj.setNationalId(rs.getInt("nationalId"));
		obj.setEmployeeNumber(rs.getInt("employeeNumber"));
		obj.setName(rs.getString("name_"));
		obj.setNationality(rs.getString("nationality"));
		obj.setGender(rs.getString("gender"));
		obj.setBirthDate(rs.getString("birthDate"));
		obj.setMobilePhone(rs.getString("mobilePhone"));
		obj.setSalary(rs.getDouble("salary"));
		obj.setJobTitle(rs.getString("jobTitle"));
		obj.setHiringDate(rs.getString("hiringDate"));
		obj.setMaritalStatus(rs.getString("maritalStatus"));
		obj.setPassportNumber(rs.getString("passportNumber"));
		obj.setSponsorName(rs.getString("sponsorName"));
		obj.setDrivingExpireDate(rs.getString("drivingExpireDate"));
		obj.setDrivingLicenceNumber(rs.getString("drivingLicenceNumber"));
		obj.setHouseAllowence(rs.getDouble("houseAllowence"));
		obj.setCareamId(rs.getString("careamId"));
		obj.setUberId(rs.getString("uberId"));
		obj.setPassword(rs.getString("password_"));
		obj.setassigned_not(rs.getInt("assigned_not"));
		
		String s = "assigned";
		if(rs.getInt("assigned_not")==0) //not assigned
			s = "not assigned";
		obj.setstatus_assigned_not(s);
		
		array.add(obj);
	}
	return array;
}

public ArrayList<EmployeeModel> getAllEmployee(/*int jtStartIndex, int jtPageSize*/) throws SQLException{
	ArrayList<EmployeeModel> array = new ArrayList<EmployeeModel>();
	
	/*String startIndex = Integer.toString(jtStartIndex);
	String pageSize = Integer.toString(jtPageSize);
	*/
	conn=DBconnection.getActiveConnection();
	//limit "+startIndex+","+pageSize
	String sql="select * from employee where assigned_not=?" ;
	PreparedStatement stmt=conn.prepareStatement(sql);
	stmt.setInt(1, 0);
	
	ResultSet rs=stmt.executeQuery();
	
	while(rs.next()){
		EmployeeModel obj=new EmployeeModel();
		obj.setNationalId(rs.getInt("nationalId"));
		obj.setEmployeeNumber(rs.getInt("employeeNumber"));
		obj.setName(rs.getString("name_"));
		obj.setNationality(rs.getString("nationality"));
		obj.setGender(rs.getString("gender"));
		obj.setBirthDate(rs.getString("birthDate"));
		obj.setMobilePhone(rs.getString("mobilePhone"));
		obj.setSalary(rs.getDouble("salary"));
		obj.setJobTitle(rs.getString("jobTitle"));
		obj.setHiringDate(rs.getString("hiringDate"));
		obj.setMaritalStatus(rs.getString("maritalStatus"));
		obj.setPassportNumber(rs.getString("passportNumber"));
		obj.setSponsorName(rs.getString("sponsorName"));
		obj.setDrivingExpireDate(rs.getString("drivingExpireDate"));
		obj.setDrivingLicenceNumber(rs.getString("drivingLicenceNumber"));
		obj.setHouseAllowence(rs.getDouble("houseAllowence"));
		obj.setCareamId(rs.getString("careamId"));
		obj.setUberId(rs.getString("uberId"));
		obj.setPassword(rs.getString("password_"));
		obj.setassigned_not(rs.getInt("assigned_not"));
		
		array.add(obj);
	}
	return array;
}


public void addNewEmployee(EmployeeModel emp) throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="insert into employee(nationalId,employeeNumber,name_,nationality,gender,birthDate,"
			+ "mobilePhone,salary,jobTitle,hiringDate,passportNumber,maritalStatus,sponsorName,"
			+ "drivingLicenceNumber,drivingExpireDate,houseAllowence,careamId,uberId,password_,assigned_not) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setInt(1,emp.getNationalId());
	stmt.setInt(2,emp.getEmployeeNumber());
	stmt.setString(3,emp.getName());
	stmt.setString(4,emp.getNationality());
	stmt.setString(5,emp.getGender());
	stmt.setString(6,emp.getBirthDate());
	stmt.setString(7,emp.getMobilePhone());
	stmt.setDouble(8,emp.getSalary());
	stmt.setString(9,emp.getJobTitle());
	stmt.setString(10,emp.getHiringDate());
	stmt.setString(11,emp.getPassportNumber());
	stmt.setString(12,emp.getMaritalStatus());
	stmt.setString(13,emp.getSponsorName());
	stmt.setString(14,emp.getDrivingLicenceNumber());
	stmt.setString(15,emp.getDrivingExpireDate());
	stmt.setDouble(16,emp.getHouseAllowence());
	stmt.setString(17,emp.getCareamId());
	stmt.setString(18,emp.getUberId());
	stmt.setString(19,emp.getPassword());
	stmt.setInt(20,0);
		
	stmt.executeUpdate();
}
//-------------------------------
public void updateEmployee(EmployeeModel emp) throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql = "update employee set employeeNumber=?,name_=?,nationality=?,"
			+ "mobilePhone=?,salary=?,jobTitle=?,passportNumber=?,maritalStatus=?,sponsorName=?,"
			+ "drivingLicenceNumber=?,drivingExpireDate=?,houseAllowence=?,careamId=?,uberId=?,password_=? where nationalId=?";
	
	PreparedStatement stmt = conn.prepareStatement(sql);
	
	stmt.setInt(1,emp.getEmployeeNumber());
	stmt.setString(2,emp.getName());
	stmt.setString(3,emp.getNationality());
	stmt.setString(4,emp.getMobilePhone());
	stmt.setDouble(5,emp.getSalary());
	stmt.setString(6,emp.getJobTitle());
	stmt.setString(7,emp.getPassportNumber());
	stmt.setString(8,emp.getMaritalStatus());
	stmt.setString(9,emp.getSponsorName());
	stmt.setString(10,emp.getDrivingLicenceNumber());
	stmt.setString(11,emp.getDrivingExpireDate());
	stmt.setDouble(12,emp.getHouseAllowence());
	stmt.setString(13,emp.getCareamId());
	stmt.setString(14,emp.getUberId());
	stmt.setString(15,emp.getPassword());
	stmt.setInt(16,emp.getNationalId());
	
	stmt.executeUpdate();
}
//-------------------------------
@Override
public String toString() {
	return "EmployeeModel [nationalId=" + nationalId + ", employeeNumber=" + employeeNumber
			+ ", name=" + name + ", nationality="+ nationality + ", salary=" +salary + ", jobTitle=" +jobTitle
			+ ", hiringDate=" +hiringDate + ", sponsorName=" +sponsorName + ", drivingLicenceNumber=" +drivingLicenceNumber
			+ ", drivingExpireDate=" +drivingExpireDate + ", houseAllowence=" +houseAllowence + ", careamId=" +careamId
			+ ", uberId=" +uberId + ", gender=" +gender + ", birthDate=" +birthDate + ", mobilePhone=" +mobilePhone
			+ ", passportNumber=" +passportNumber + ", maritalStatus=" +maritalStatus + ", password_=" +password +"]";
}
//-------------------------------
public void  deleteEmployee(int NationalId) throws SQLException{
	
	un_assignEmployee(NationalId);
	
	conn=DBconnection.getActiveConnection();
	String sql="delete from employee where nationalId= ?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setInt(1,NationalId);	
	
	stmt.executeUpdate();
	
}
//-------------------------------
//-------------------------------
public int  deleteEmployee_(int NationalId) throws SQLException{
	
	conn=DBconnection.getActiveConnection();
	String sql="delete from employee where nationalId= ?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setInt(1,nationalId);	
	
	return stmt.executeUpdate();
	}

public boolean checkDriverAssign(int nationalId) throws SQLException{
	conn=DBconnection.getActiveConnection();
	String sql="select * from employee where nationalId= ? and assigned_not=?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setInt(1,nationalId);
	stmt.setInt(2,0);
	ResultSet rs = stmt.executeQuery();
	while(rs.next()){
		return true; //not assigned
	}
	 
	return false;
}
//------------------
public int updateAssignStatus(int nID) throws SQLException{
	if(checkDriverAssign(nID)) //already assigned
		return -1;
	
	conn=DBconnection.getActiveConnection();
	String sql="update employee set assigned_not=? where nationalId= ?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setInt(1,1);
	stmt.setInt(2,nID);	
	
	return stmt.executeUpdate();
}

public int un_assignEmployee(int nID) throws SQLException{
	if(checkDriverAssign(nID)) //not assigned
		return -1;
	
	conn=DBconnection.getActiveConnection();
	String sql="update car set assigned_not=? where carDriverNationalId= ?";
	PreparedStatement stmt=conn.prepareStatement(sql);
	
	stmt.setInt(1,0);
	stmt.setInt(2,nID);	
	
	return stmt.executeUpdate();
}


}

