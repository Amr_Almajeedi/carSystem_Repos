package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CompanyTypeModel {

	private String carType;
	private String financeCompanyName;
	private String description;
	private Connection conn=null;
	public CompanyTypeModel(){}

	/**
	 * @return the carType
	 */
	public String getCarType() {
		return carType;
	}

	/**
	 * @param carType the carType to set
	 */
	public void setCarType(String carType) {
		this.carType = carType;
	}

	/**
	 * @return the financeCompanyName
	 */
	public String getFinanceCompanyName() {
		return financeCompanyName;
	}

	/**
	 * @param financeCompanyName the financeCompanyName to set
	 */
	public void setFinanceCompanyName(String financeCompanyName) {
		this.financeCompanyName = financeCompanyName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public int addNewCompanyType(String carType,String financeCompanyName,String description) throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="insert into financecompanytype(carType,financeCompanyName,description) values(?,?,?)";
		PreparedStatement stmt=conn.prepareStatement(sql);
		 
		stmt.setString(1,carType);
		stmt.setString(2,financeCompanyName);
		stmt.setString(3,description);
		
		return stmt.executeUpdate();
	}
	
	
	public ArrayList<CompanyTypeModel> getAllTypes() throws SQLException{
		ArrayList<CompanyTypeModel> types = new ArrayList<CompanyTypeModel>();
		
		conn=DBconnection.getActiveConnection();
		String sql="select * from financecompanytype";
		PreparedStatement stmt=conn.prepareStatement(sql);
		ResultSet rs=stmt.executeQuery();
		
		CompanyTypeModel obj=new CompanyTypeModel();
		while(rs.next()){
			obj.setCarType(rs.getString("carType"));
			obj.setDescription(rs.getString("description"));
			obj.setFinanceCompanyName(rs.getString("financeCompanyName"));
			
			types.add(obj);
		}
		return types;
	}
	  
public ArrayList<CompanyTypeModel> companyReport(String name) throws SQLException{
		
		conn=DBconnection.getActiveConnection();
		String sql="select * from financecompanytype where financeCompanyName=?";
		PreparedStatement stmt=conn.prepareStatement(sql);
		stmt.setString(1,name);
		ResultSet rs=stmt.executeQuery();
		
		
		ArrayList<CompanyTypeModel> array=new ArrayList<CompanyTypeModel>();
		while (rs.next()){
			CompanyTypeModel obj=new CompanyTypeModel();
			obj.setCarType(rs.getString("carType"));
			obj.setFinanceCompanyName(rs.getString("financeCompanyName"));
			obj.setDescription(rs.getString("description"));
			
			array.add(obj);
			
		}
		
		return array;
}
}
