package services;

import java.sql.SQLException;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import model.EmployeeModel;
import model.FinanceCompanyModel;

import org.json.simple.JSONObject;

@Path("/FinanceCompany")
public class FinanceCompany {

	/*String companyName,int numberOfCars*/
	@POST
	@Path("/addNewFinanceCompany")
	@Produces(MediaType.TEXT_PLAIN)
	public String addNewFinanceCompany(@FormParam("companyName") String companyName,@FormParam("numberOfCars") String numberOfCars) throws NumberFormatException, SQLException {
		
	FinanceCompanyModel com=new FinanceCompanyModel();
	
	int result=com.addNewFinanceCompany(companyName,Integer.parseInt(numberOfCars));
	String str="";
	//String res="result";
	JSONObject json=new JSONObject();
	if (result!=0){
		str="Company Added Successful";
		
	}
	else{
		str="Company is Already Exist";
	}
		json.put("result", str);
		
		return json.toString();
	}

	@POST
	@Path("/getNumberOfCars")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject getNumberOfCars(@FormParam("name") String name) throws NumberFormatException, SQLException {
		
	FinanceCompanyModel com=new FinanceCompanyModel();
	
	int result=com.getNumberOfCars(name);
	String str="";
	JSONObject json=new JSONObject();
	if (result!=-1){
		json.put("result", result);
	}
	else{
		str="Company not Exist";
		json.put("result", str);
	}
		
		return json;
	}
	
	@POST
	@Path("/updateNumberOfCars")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject updateNumberOfCars(@FormParam("name") String name,@FormParam("num") String num) throws NumberFormatException, SQLException {
		
	FinanceCompanyModel com=new FinanceCompanyModel();
	
	int result=com.updateNumberOfCars(name,Integer.parseInt(num));
	String str="";
	JSONObject json=new JSONObject();
	if (result!=0){
		str="NumberCars update successful";
	}
	else{
		str="Company not Exist";
		
	}
	json.put("result", str);
		return json;
	}
}
