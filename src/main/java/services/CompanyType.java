package services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import model.CompanyTypeModel;
import model.EmployeeModel;

import org.json.simple.JSONObject;

@Path("/CompanyType")
public class CompanyType {

	/*String carType,String financeCompanyName,String description*/
	@POST
	@Path("/addNewCompanyType")
	@Produces(MediaType.TEXT_PLAIN)
	public String addNewCompanyType(@FormParam("carType") String carType,@FormParam("financeCompanyName") String financeCompanyName
			,@FormParam("description") String description) throws NumberFormatException, SQLException {
		
	CompanyTypeModel company=new CompanyTypeModel();
	
	int result=company.addNewCompanyType(carType, financeCompanyName, description);
	String str="";
	String res="result";
	JSONObject json=new JSONObject();
	if (result!=0){
		str="Company type Added Successful";
		
	}
	else{
		str="Error in add";
	}
		json.put("result", str);
		
		return json.toString();
	}
	@POST
	@Path("/companyReport")
	@Produces(MediaType.TEXT_PLAIN)
	public String companyReport(@FormParam("companyName") String companyName) throws NumberFormatException, SQLException {
		
	ArrayList<CompanyTypeModel>array =null;
	CompanyTypeModel com=new CompanyTypeModel();
	array=com.companyReport(companyName);
	String str="";
	JSONObject json=new JSONObject();
	if (com!=null){
		json.put("result", array);
	}
	else{
		str="Company type not exist";
		json.put("result", str);
	}
		
	
		return json.toString();
	}
	
}
