package services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import model.FineModel;
import model.IncomeModel;

import org.json.simple.JSONObject;
/*String carNumber,int driverNationalId,String date,
			String incomType,double revenue*/
@Path("/Income")
public class Income {

	@POST
	@Path("/addNewIncome")
	@Produces(MediaType.TEXT_PLAIN)
	public String addNewIncome(@FormParam("carNumber") String carNumber,@FormParam("driverNationalId") String driverNationalId
			,@FormParam("date") String date,@FormParam("incomType") String incomType
			,@FormParam("revenue") String revenue) throws NumberFormatException, SQLException {
		
	IncomeModel income=new IncomeModel();
	
	int result=income.addNewIncome(carNumber,Integer.parseInt(driverNationalId) , date, incomType,Double.parseDouble(revenue));
	//String str="";
	//String res="result";
	JSONObject json=new JSONObject();
	//if (result!=0){
		//str="Income Added Successful";
		
	//}
	//else{
		//str="Income is Already Exist";
	//}
		json.put("result", result);
		
		return json.toString();
	}
	@POST
	@Path("/IncomeReport")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject IncomeReport(@FormParam("carNumber") String carNumber) throws NumberFormatException, SQLException {
		
	ArrayList<IncomeModel>array =null;
	IncomeModel income=new IncomeModel();
	array=income.IncomeReport(carNumber);
	String str="";
	JSONObject json=new JSONObject();
	if (income!=null){
		json.put("result", array);
	}
	else{
		str="Income Not Exist";
		json.put("result", str);
	}
		
	
		return json;
	}
	@POST
	@Path("/totalIncome")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject totalIncome(@FormParam("carNumber") String carNumber) throws NumberFormatException, SQLException {
		
	IncomeModel income=new IncomeModel();
	
	double result=income.totalIncome(carNumber);
	String str="";
	JSONObject json=new JSONObject();
	if (result!=-1){
		json.put("result", result);
	}
	else{
		str="Income Not Exist";
		json.put("result", str);
	}
		
	
		return json;
	}
	
	@POST
	@Path("/employeeTotalIncome")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject employeeTotalIncome(@FormParam("nationalId") String nationalId) throws NumberFormatException, SQLException {
		
	IncomeModel income=new IncomeModel();
	
	double result=income.employeeTotalIncome(Integer.parseInt(nationalId));
	String str="";
	JSONObject json=new JSONObject();
	if (result!=-1){
		json.put("result", result);
	}
	else{
		str="Income Not Exist";
		json.put("result", str);
	}
		
	
		return json;
	}
	
	@POST
	@Path("/dayTotalIncome")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject dayTotalIncome(@FormParam("date") String date) throws NumberFormatException, SQLException {
		
	IncomeModel income=new IncomeModel();
	
	double result=income.dayTotalIncome(date);
	String str="";
	JSONObject json=new JSONObject();
	if (result!=-1){
		json.put("result", result);
	}
	else{
		str="Income Not Exist";
		json.put("result", str);
	}
		
	
		return json;
	}
	
	@POST
	@Path("/dayIncome")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject dayIncome(@FormParam("carNumber") String carNumber,@FormParam("date") String date) throws NumberFormatException, SQLException {
		
	IncomeModel income=new IncomeModel();
	
	double result=income.dayIncome(carNumber, date);
	String str="";
	JSONObject json=new JSONObject();
	if (result!=-1){
		json.put("result", result);
	}
	else{
		str="Income Not Exist";
		json.put("result", str);
	}
		
		return json;
	}
	
	@POST
	@Path("/IncomeForPeroid")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject IncomeForPeroid(@FormParam("carNumber") String carNumber
			,@FormParam("fromDate") String fromDate,@FormParam("toDate") String toDate) throws NumberFormatException, SQLException {
		
	IncomeModel income=new IncomeModel();
	
	double result=income.IncomeForPeroid(carNumber, fromDate,toDate);
	String str="";
	JSONObject json=new JSONObject();
	if (result!=-1){
		json.put("result", result);
	}
	else{
		str="Income Not Exist";
		json.put("result", str);
	}
		
		return json;
	}
	
	@POST
	@Path("/IncomeFromType")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject IncomeFromType(@FormParam("carNumber") String carNumber
			,@FormParam("type") String type,@FormParam("date") String date) throws NumberFormatException, SQLException {
		
	IncomeModel income=new IncomeModel();
	
	double result=income.IncomeFromType(carNumber, type, date);
	String str="";
	JSONObject json=new JSONObject();
	if (result!=-1){
		json.put("result", result);
	}
	else{
		str="Income Not Exist";
		json.put("result", str);
	}
		
		return json;
	}
}
