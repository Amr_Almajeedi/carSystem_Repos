package services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import model.EmployeeModel;
import model.FineModel;

import org.json.simple.JSONObject;

@Path("/Fine")
public class Fine {

	/*String fineType,String description,String carNumber,int driverNationalId,
			double fineAmount,String fineDate*/
	@POST
	@Path("/addNewFine")
	@Produces(MediaType.TEXT_PLAIN)
	public String addNewFine(@FormParam("fineType") String fineType,@FormParam("description") String description
			,@FormParam("carNumber") String carNumber,@FormParam("driverNationalId") String driverNationalId
			,@FormParam("fineAmount") String fineAmount, @FormParam("fineDate") String fineDate) throws NumberFormatException, SQLException {
		
	FineModel fine=new FineModel();
	
	int result=fine.addNewFine(fineType, description, carNumber,Integer.parseInt(driverNationalId) ,
			Double.parseDouble(fineAmount) , fineDate);
	//String str="";
	JSONObject json=new JSONObject();
	//if (result!=0){
		//str="Fine Added Successful";
		
	//}
	//else{
		//str="Fial to add fine";
	//}
		json.put("result", result);
		
		return json.toString();
	}
	
	@POST
	@Path("/fineReport")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject fineReport(@FormParam("carNumber") String carNumber) throws NumberFormatException, SQLException {
		
	ArrayList<FineModel>array =null;
	FineModel fine=new FineModel();
	array=fine.fineReport(carNumber);
	String str="";
	JSONObject json=new JSONObject();
	if (fine!=null){
		json.put("result", array);
	}
	else{
		str="Fine Not Exist";
		json.put("result", str);
	}
		
	
		return json;
	}
	
	@POST
	@Path("/fineReportByDriver")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject fineReportByDriver(@FormParam("nationalId") String nationalId) throws NumberFormatException, SQLException {
		
	ArrayList<FineModel>array =null;
	FineModel fine=new FineModel();
	array=fine.fineReportByDriver(Integer.parseInt(nationalId));
	String str="";
	JSONObject json=new JSONObject();
	if (fine!=null){
		json.put("result", array);
	}
	else{
		str="Fine Not Exist";
		json.put("result", str);
	}
		
	
		return json;
	}
	
	@POST
	@Path("/totalFines")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject totalFines(@FormParam("carNumber") String carNumber) throws NumberFormatException, SQLException {
		
	FineModel fine=new FineModel();
	
	double result=fine.totalFines(carNumber);
	String str="";
	JSONObject json=new JSONObject();
	if (result!=-1){
		json.put("result", result);
	}
	else{
		str="Fine Not Exist";
		json.put("result", str);
	}
		
	
		return json;
	}
	@POST
	@Path("/employeeTotalFines")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject employeeTotalFines(@FormParam("nationalId") String nationalId) throws NumberFormatException, SQLException {
		
	FineModel fine=new FineModel();
	
	double result=fine.employeeTotalFines(Integer.parseInt(nationalId));
	String str="";
	JSONObject json=new JSONObject();
	if (result!=-1){
		json.put("result", result);
	}
	else{
		str="Fine Not Exist";
		json.put("result", str);
	}
		
	
		return json;
	}
	
	@POST
	@Path("/dayTotalFines")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject dayTotalFines(@FormParam("date") String date) throws NumberFormatException, SQLException {
		
	FineModel fine=new FineModel();
	
	double result=fine.dayTotalFines(date);
	String str="";
	JSONObject json=new JSONObject();
	if (result!=-1){
		json.put("result", result);
	}
	else{
		str="Fine Not Exist";
		json.put("result", str);
	}
		
	
		return json;
	}
	
	@POST
	@Path("/totalFinesForPeroid")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject totalFinesForPeroid(@FormParam("carNumber") String carNumber,@FormParam("fromDate") String fromDate
			,@FormParam("toDate") String toDate) throws NumberFormatException, SQLException {
		
	FineModel fine=new FineModel();
	
	double result=fine.totalFinesForPeroid(carNumber, fromDate, toDate);
	String str="";
	JSONObject json=new JSONObject();
	if (result!=-1){
		json.put("result", result);
	}
	else{
		str="Fine Not Exist";
		json.put("result", str);
	}
		
	
		return json;
	}
}
