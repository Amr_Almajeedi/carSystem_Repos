package services;

import java.sql.SQLException;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import model.CarModel;
import model.EmployeeModel;

import org.json.simple.JSONObject;

import com.mysql.jdbc.Driver;

/*
 * String carNumber,String type,String model,String style,String color,
			int carDriverNationalId,double countKm,double monthlyInstallment,double totalLoan,
			double remainLoan,String firstInstallmentDate,String carStatus,String paymentType,
			String carCompanyName,String maintainanceComponyName,String imageUrl,double totalFines
 * */
@Path("/Car")
public class Car {

	@POST
	@Path("/addNewCar")
	@Produces(MediaType.TEXT_PLAIN)
	public String addNewCar(@FormParam("carNumber") String carNumber,@FormParam("type") String type,@FormParam("model") String model,
			@FormParam("style") String style,@FormParam("color") String color, @FormParam("carDriverNationalId") String carDriverNationalId,
			@FormParam("countKm") String countKm,@FormParam("monthlyInstallment") String monthlyInstallment,@FormParam("totalLoan") String totalLoan,
			@FormParam("remainLoan") String remainLoan,@FormParam("firstInstallmentDate") String firstInstallmentDate,@FormParam("carStatus") String carStatus,
			@FormParam("paymentType") String paymentType,@FormParam("carCompanyName") String carCompanyName,
			/*@FormParam("imageUrl") String imageUrl,*/@FormParam("totalFines") String totalFines) throws NumberFormatException, SQLException {
		
	CarModel car=new CarModel(); 
	EmployeeModel emp = new EmployeeModel();
	
	int car_assigned_not=0;
	
	if(carDriverNationalId.length()!=0){
		car_assigned_not = 1;
		int res = emp.updateAssignStatus(Integer.parseInt(carDriverNationalId));
		if(res==-1){
			// driver already assigned to a car
			car_assigned_not = 0;
		}
	}else{
		carDriverNationalId = "0";
	}
	 
	int result=car.addNewCar(carNumber, type, model, style, color,Integer.parseInt(carDriverNationalId) ,Double.parseDouble(countKm) ,
			Double.parseDouble(monthlyInstallment),Double.parseDouble(totalLoan),Double.parseDouble(remainLoan) , 
			firstInstallmentDate, carStatus, paymentType, carCompanyName,/*imageUrl,*/Double.parseDouble(totalFines),car_assigned_not );
	String str="";
	String res="result";
	JSONObject json=new JSONObject();
	if (result!=0){
		str="Car Added Successful";
		json.put("result", 1);
		
	}
	else{
		str="Car is Already Exist";
		json.put("result", -1);
	}
		//json.put("result", str);
		
		return json.toString();
	}
	
	@POST
	@Path("/carReport")
	@Produces(MediaType.TEXT_PLAIN)
	public String carReport(@FormParam("carNumber") String carNumber) throws NumberFormatException, SQLException {
		
	CarModel car=new CarModel();
	EmployeeModel driverReport=null;
	EmployeeModel emp = new EmployeeModel();
	
	CarModel carReport=car.carReport(carNumber);
	String str="";
	JSONObject json=new JSONObject();

	if(car!=null){
		driverReport = emp.employeeReport(carReport.getCarDriverNationalId());
		//if (car!=null){
	
		json.put("result", 1);

		json.put("carReport_carNumber", carReport.getCarNumber());
		json.put("carReport_carDriverNationalId", carReport.getCarDriverNationalId());
		json.put("carReport_countKm", carReport.getCountKm());
		json.put("carReport_carCompanyName", carReport.getCarCompanyName());
		json.put("carReport_model", carReport.getModel());
		json.put("carReport_type_", carReport.getType());
		json.put("carReport_style", carReport.getStyle());
		json.put("carReport_carStatus", carReport.getCarStatus());
		json.put("carReport_paymentType", carReport.getPaymentType());
		json.put("carReport_remainLoan", carReport.getRemainLoan());
		json.put("carReport_totalLoan", carReport.getTotalLoan());
		json.put("carReport_firstInstallmentDate", carReport.getFirstInstallmentDate());
		json.put("carReport_monthlyInstallment", carReport.getMonthlyInstallment());
		//json.put("carReport_image", carReport.getImageUrl());
		json.put("carReport_color", carReport.getColor());
		json.put("carReport_totalFines", carReport.getTotalFines());
		json.put("assigned_not", carReport.getassigned_not());
		
		
		json.put("driverReport_nationalId",driverReport.getNationalId());
		json.put("driverReport_employeeNumber",driverReport.getEmployeeNumber());
		json.put("driverReport_name_",driverReport.getName());
		json.put("driverReport_nationality",driverReport.getNationality());
		json.put("driverReport_gender",driverReport.getGender());
		json.put("driverReport_birthDate",driverReport.getBirthDate());
		json.put("driverReport_salary",driverReport.getSalary());
		json.put("driverReport_mobilePhone",driverReport.getMobilePhone());
		json.put("driverReport_jobTitle",driverReport.getJobTitle());
		json.put("driverReport_hiringDate",driverReport.getHiringDate());
		json.put("driverReport_passportNumber",driverReport.getPassportNumber());
		json.put("driverReport_maritalStatus",driverReport.getMaritalStatus());
		json.put("driverReport_sponsorName",driverReport.getSponsorName());
		json.put("driverReport_drivingLicenceNumber",driverReport.getDrivingLicenceNumber());
		json.put("driverReport_drivingExpireDate",driverReport.getDrivingExpireDate());
		json.put("driverReport_houseAllowence",driverReport.getHouseAllowence());
		json.put("driverReport_careamId",driverReport.getCareamId());
		json.put("driverReport_uberId",driverReport.getUberId());
		json.put("driverReport_password_",driverReport.getPassword());
	}
	else{
		json.put("result", -1);
	}
		
		
		return json.toString();
	}
	
	@POST
	@Path("/carReportByDriver")
	@Produces(MediaType.TEXT_PLAIN)
	public String carReportByDriver(@FormParam("nationalId") String nationalId) throws NumberFormatException, SQLException {
		
	CarModel car=null;
	
	car=car.carReportByDriver(Integer.parseInt(nationalId));
	String str="";
	JSONObject json=new JSONObject();
	if (car!=null){
		json.put("result", car);
	}
	//}
	//else{
		//str="Car Not Exist";
		//json.put("result", str);
	//}
		
		
		return json.toString();
	}
	
	@POST
	@Path("/updateCountKm")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject updateCountKm(@FormParam("carNumber") String carNumber,@FormParam("countKm") String countKm) throws NumberFormatException, SQLException {
		
	CarModel car=new CarModel();
	
	int result=car.updateCountKm(carNumber,Double.parseDouble(countKm) );
	String str="";
	JSONObject json=new JSONObject();
	if (result!=0){
		str="KM Updated Successful";
		
	}
	else{
		str="Car Not Exist";
	}
		
	json.put("result", str);
		return json;
	}
	
	@POST
	@Path("/UpdateCarDriver")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject UpdateCarDriver(@FormParam("carNumber") String carNumber,@FormParam("nationalId") String nationalId) throws NumberFormatException, SQLException {
		
	CarModel car=new CarModel();
	
	int result=car.UpdateCarDriver(carNumber,Integer.parseInt(nationalId) );
	//String str="";
	JSONObject json=new JSONObject();
//	if (result!=0){
//		str="Driver Updated Successful";
//		
//	}
//	else{
//		str="Car Not Exist";
//	}
		
	json.put("result", result);//-1(national not found) or 1
		return json;
	}
	
	@POST
	@Path("/updateRemainLoan")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject updateRemainLoan(@FormParam("carNumber") String carNumber,@FormParam("amount") String amount) throws NumberFormatException, SQLException {
		
	CarModel car=new CarModel();
	
	int result=car.updateRemainLoan(carNumber,Double.parseDouble(amount) );
	String str="";
	JSONObject json=new JSONObject();
	if (result!=0){
		str="Loan Updated Successful";
		
	}
	else{
		str="Car Not Exist";
	}
		
	json.put("result", str);
		return json;
	}
	
	@POST
	@Path("/updateCarStatus")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject updateCarStatus(@FormParam("carNumber") String carNumber,@FormParam("status") String status) throws NumberFormatException, SQLException {
		
	CarModel car=new CarModel();
	
	int result=car.updateCarStatus(carNumber, status);
	String str="";
	JSONObject json=new JSONObject();
	if (result!=0){
		str="Status Updated Successful";
		
	}
	else{
		str="Car Not Exist";
	}
		
	json.put("result", str);
		return json;
	}
	@POST
	@Path("/updateTotalFines")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject updateTotalFines(@FormParam("carNumber") String carNumber,@FormParam("amount") String amount) throws NumberFormatException, SQLException {
		
	CarModel car=new CarModel();
	
	int result=car.updateTotalFines(carNumber,Double.parseDouble(amount) );
	String str="";
	JSONObject json=new JSONObject();
	if (result!=0){
		str="Fine Updated Successful";
		
	}
	else{
		str="Car Not Exist";
	}
		
	json.put("result", str);
		return json;
	}
	
	//----------

	@POST
	@Path("/updateCar")
	@Produces(MediaType.TEXT_PLAIN)
	public String updateCar(@FormParam("carNumber") String carNumber,
			/*@FormParam("carDriverNationalId") String carDriverNationalId,*/@FormParam("countKm") String countKm
			,@FormParam("remainLoan") String remainLoan,@FormParam("carStatus") String carStatus,
			@FormParam("paymentType") String paymentType/*,@FormParam("imageUrl") String imageUrl*/) throws NumberFormatException, SQLException {
		
	CarModel car=new CarModel();
	
	int result=car.updateCar(carNumber, /*Integer.parseInt(carDriverNationalId),*/ Double.parseDouble(countKm), Double.parseDouble(remainLoan), carStatus, paymentType/*, imageUrl*/);
	String str="";
	JSONObject json=new JSONObject();
//	if (result!=0){
//		str="Fine Updated Successful";
		
//	}
//	else{
//		str="Car Not Exist";
//	}
		
	json.put("result", result);
		return json.toString();
	}
	//----------
	
	@POST
	@Path("/deleteCar")
	@Produces(MediaType.TEXT_PLAIN)
	public String deleteCar(@FormParam("carNumber") String carNumber) throws NumberFormatException, SQLException {
		
	CarModel car=new CarModel();
	
	int result=car.deleteCar(carNumber);
	//String str="";
	JSONObject json=new JSONObject();
//	if (result!=0){
//		str="Car Deleted Successful";
//		
//	}
//	else{
//		str="Car Not Exist";
//	}
		
	json.put("result", result);
		return json.toString();
	}
}
