package services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import model.EmployeeModel;
import model.IncomeModel;
import model.MaintainanceModel;

import org.json.simple.JSONObject;

@Path("/Maintainance")
public class Maintainance {

	/*String companyName,String carNumber,String serviceType,double serviceCost,
			String serviceDate,String nextServiceDate*/
	@POST
	@Path("/addNewMaintainance")
	@Produces(MediaType.TEXT_PLAIN)
	public String addNewMaintainance(@FormParam("companyName") String companyName,@FormParam("carNumber") String carNumber,
			@FormParam("serviceType") String serviceType,@FormParam("serviceCost") String serviceCost,
			@FormParam("serviceDate") String serviceDate, @FormParam("nextServiceDate") String nextServiceDate) throws NumberFormatException, SQLException {
		
	MaintainanceModel maintain=new MaintainanceModel();
	
	int result=maintain.addNewMaintainance(companyName, carNumber, serviceType,Double.parseDouble(serviceCost) , serviceDate, nextServiceDate);
//	String str="";
//	String res="result";
	JSONObject json=new JSONObject();
//	if (result!=0){
//		str="Maintainance Added Successful";
//		
//	}
//	else{
//		str="Car not exist!";
//	}
		json.put("result", result);
		
		return json.toString();
	}
	@POST
	@Path("/totalCostForPeroid")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject totalCostForPeroid(@FormParam("carNumber") String carNumber
			,@FormParam("fromDate") String fromDate,@FormParam("toDate") String toDate) throws NumberFormatException, SQLException {
		
	MaintainanceModel main=new MaintainanceModel();
	
	double result=main.totalCostForPeroid(carNumber, fromDate, toDate);
	String str="";
	JSONObject json=new JSONObject();
	if (result!=-1){
		json.put("result", result);
	}
	else{
		str="Maintainance Not Exist";
		json.put("result", str);
	}
		
		return json;
	}
	@POST
	@Path("/ServiceCost")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject ServiceCost(@FormParam("carNumber") String carNumber,@FormParam("date") String date) throws NumberFormatException, SQLException {
		
	MaintainanceModel main=new MaintainanceModel();
	
	double result=main.ServiceCost(carNumber, date);
	String str="";
	JSONObject json=new JSONObject();
	if (result!=-1){
		json.put("result", result);
	}
	else{
		str="Maintainance Not Exist";
		json.put("result", str);
	}
		
		return json;
	}
	@POST
	@Path("/maintainanceReport")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject maintainanceReport(@FormParam("carNumber") String carNumber) throws NumberFormatException, SQLException {
		
	ArrayList<MaintainanceModel>array =null;
	MaintainanceModel main=new MaintainanceModel();
	array=main.maintainanceReport(carNumber);
	String str="";
	JSONObject json=new JSONObject();
	if (main!=null){
		json.put("result", array);
	}
	else{
		str="Maintainance Not Exist";
		json.put("result", str);
	}
		
	
		return json;
	}
	
}
