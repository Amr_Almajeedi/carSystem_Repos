package services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.simple.JSONObject;

import model.EmployeeModel;

/*
 * int nationalId,int employeeNumber,String name,String nationality,String gender,
			String birthDate,String mobilePhone,double salary,String jobTitle,String hiringDate,
			String passportNumber,String maritalStatus,String sponsorName,String drivingLicenceNumber,
			String drivingExpireDate,double houseAllowence,String careamId,String uberId,String password
 * */
@Path("/Employee")
public class Employee {
	
	@POST
	@Path("/addNewEmployee")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject addNewEmployee(@FormParam("nationalId") String nationalId,@FormParam("employeeNumber") String employeeNumber,@FormParam("name") String name,
			@FormParam("nationality") String nationality,@FormParam("gender") String gender, @FormParam("birthDate") String birthDate,
			@FormParam("mobilePhone") String mobilePhone,@FormParam("salary") String salary,@FormParam("jobTitle") String jobTitle,
			@FormParam("hiringDate") String hiringDate,@FormParam("passportNumber") String passportNumber,@FormParam("maritalStatus") String maritalStatus,
			@FormParam("sponsorName") String sponsorName,@FormParam("drivingLicenceNumber") String drivingLicenceNumber,
			@FormParam("drivingExpireDate") String drivingExpireDate,@FormParam("houseAllowence") String houseAllowence,
			@FormParam("careamId") String careamId,@FormParam("uberId") String uberId,@FormParam("password") String password) throws NumberFormatException, SQLException {
		
	EmployeeModel emp=new EmployeeModel();
	
	int result=emp.addNewEmployee(Integer.parseInt(nationalId) ,Integer.parseInt(employeeNumber), name, nationality, gender, 
			birthDate, mobilePhone,Double.parseDouble(salary),jobTitle, hiringDate, passportNumber, maritalStatus, 
			sponsorName, drivingLicenceNumber, drivingExpireDate,Double.parseDouble(houseAllowence), careamId, uberId, password);
	String str="";
	String res="result";
	JSONObject json=new JSONObject();
	if (result!=0){
		str="Employee Added Successful";
		
	}
	else{
		str="Employee is Already Exist";
	}
		json.put("result", str);
		
		return json;
	}
	
	@POST
	@Path("/employeeReport")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject employeeReport(@FormParam("nationalId") String nationalId) throws NumberFormatException, SQLException {
		
	EmployeeModel emp=null;
	
	emp=emp.employeeReport(Integer.parseInt(nationalId));
	String str="";
	JSONObject json=new JSONObject();
	if (emp!=null){
		json.put("result", emp);
	}
	else{
		str="Employee not exist";
		json.put("result", str);
	}
		
		
		return json;
	}
	
	//------------------------------------
	@POST
	@Path("/logIn")
	@Produces(MediaType.TEXT_PLAIN)
	public String logIn(@FormParam("nationalId") String nationalId,@FormParam("password") String password) throws NumberFormatException, SQLException {
		
	EmployeeModel emp=new EmployeeModel();
	
	emp=emp.logIn(Integer.parseInt(nationalId),password);
	JSONObject json=new JSONObject();
	if (emp!=null){
		json.put("result", emp.getNationalId());
	}
	else{
		//str="Employee not exist";
		json.put("result", -1);
	}
		
		return json.toString();
	}
	//------------------------------------
	
	@POST
	@Path("/updateSalary")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject updateSalary(@FormParam("nationalId") String nationalId,@FormParam("salary") String salary) throws NumberFormatException, SQLException {
		
	EmployeeModel emp=new EmployeeModel();
	
	int result=emp.updateSalary(Integer.parseInt(nationalId),Double.parseDouble(salary));
	String str="";
	JSONObject json=new JSONObject();
	if (result!=0){
		str="Salary Updated Successful";
		
	}
	else{
		str="Employee Not Exist";
	}
		
	json.put("result", str);
		return json;
	}
	
	@POST
	@Path("/updateJob")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject updateJob(@FormParam("nationalId") String nationalId,@FormParam("job") String job) throws NumberFormatException, SQLException {
		
	EmployeeModel emp=new EmployeeModel();
	
	int result=emp.updateJob(Integer.parseInt(nationalId),job);
	String str="";
	JSONObject json=new JSONObject();
	if (result!=0){
		str="Job Updated Successful";
		
	}
	else{
		str="Employee Not Exist";
	}
		
	json.put("result", str);
		return json;
	}
	
	@POST
	@Path("/updateMaritalStatus")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject updateMaritalStatus(@FormParam("nationalId") String nationalId,@FormParam("status") String status) throws NumberFormatException, SQLException {
		
	EmployeeModel emp=new EmployeeModel();
	
	int result=emp.updateMaritalStatus(Integer.parseInt(nationalId), status);
	String str="";
	JSONObject json=new JSONObject();
	if (result!=0){
		str="Status Updated Successful";
		
	}
	else{
		str="Employee Not Exist";
	}
		
	json.put("result", str);
		return json;
	}

	@POST
	@Path("/updateSponsorName")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject updateSponsorName(@FormParam("nationalId") String nationalId,@FormParam("sponsorName") String sponsorName) throws NumberFormatException, SQLException {
		
	EmployeeModel emp=new EmployeeModel();
	
	int result=emp.updateSponsorName(Integer.parseInt(nationalId),sponsorName);
	String str="";
	JSONObject json=new JSONObject();
	if (result!=0){
		str="sponsorName Updated Successful";
		
	}
	else{
		str="Employee Not Exist";
	}
		
	json.put("result", str);
		return json;
	}
	@POST
	@Path("/updateHouseAllownce")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject updateHouseAllownce(@FormParam("nationalId") String nationalId,@FormParam("houseAllownce") String houseAllownce) throws NumberFormatException, SQLException {
		
	EmployeeModel emp=new EmployeeModel();
	
	int result=emp.updateHouseAllownce(Integer.parseInt(nationalId),Double.parseDouble(houseAllownce));
	String str="";
	JSONObject json=new JSONObject();
	if (result!=0){
		str="sponsorName Updated Successful";
		
	}
	else{
		str="Employee Not Exist";
	}
		
	json.put("result", str);
		return json;
	}
	@POST
	@Path("/updateMobile")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject updateMobile(@FormParam("nationalId") String nationalId,@FormParam("mobile") String mobile) throws NumberFormatException, SQLException {
		
	EmployeeModel emp=new EmployeeModel();
	
	int result=emp.updateMobile(Integer.parseInt(nationalId),mobile);
	String str="";
	JSONObject json=new JSONObject();
	if (result!=0){
		str="sponsorName Updated Successful";
		
	}
	else{
		str="Employee Not Exist";
	}
		
	json.put("result", str);
		return json;
	}
	
	@POST
	@Path("/getEmployeeByJob")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject getEmployeeByJob(@FormParam("job") String job) throws NumberFormatException, SQLException {
		
	ArrayList<EmployeeModel>array =null;
	EmployeeModel emp=new EmployeeModel();
	array=emp.getEmployeeByJob(job);
	String str="";
	JSONObject json=new JSONObject();
	if (emp!=null){
		json.put("result", array);
	}
	else{
		str="Employee Not Exist";
		json.put("result", str);
	}
		
	
		return json;
	}
	
	@POST
	@Path("/deleteEmployee")
	@Produces(MediaType.TEXT_PLAIN)
	public JSONObject deleteEmployee(@FormParam("nationalId") String nationalId) throws NumberFormatException, SQLException {
		
	EmployeeModel emp=new EmployeeModel();
	
	int result=emp.deleteEmployee_(Integer.parseInt(nationalId));
	String str="";
	JSONObject json=new JSONObject();
	if (result!=0){
		str="sponsorName Deleted Successful";
		
	}
	else{
		str="Employee Not Exist";
	}
		
	json.put("result", str);
		return json;
	}
}
