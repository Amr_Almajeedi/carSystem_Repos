package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.ws.rs.FormParam;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import model.CarModel;
import model.DB;
import model.DBconnection;
import model.EmployeeModel;

/**
 * Servlet implementation class AddNewCar
 */
@WebServlet("/AddNewCar")
@MultipartConfig(maxFileSize = 16177215)    // upload file's size up to 16MB
public class AddNewCar extends HttpServlet {
	private static final long serialVersionUID = 1L;
        
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddNewCar() {
        super();
        // TODO Auto-generated constructor stub
    } 

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	public void insertImage(InputStream inputStream, String carNumber) throws IOException{
        Connection conn = null; // connection to the database
        //response.getWriter().print(inputStream + "     " + carNumber);
        try {
            // connects to the database
           // DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            conn = DBconnection.getActiveConnection();
 
            // constructs SQL statement
            String sql = "update car set stream=? where carNumber= ?";
            PreparedStatement statement;
                    try {
                        statement = conn.prepareStatement(sql);
                        //statement.setString(1, "try");
                        statement.setString(2, carNumber);
                        if (inputStream != null) {
                            statement.setBinaryStream(1, inputStream,1000000);
                        }
                        int row = statement.executeUpdate();
                        if (row > 0) {
                        	
                            //message = "File uploaded and saved into database";
                            //response.getWriter().print(message);
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(ImageController.class.getName()).log(Level.SEVERE, null, ex);
                    }
         
             }catch (Exception ex) {
                //message = "ERROR: " + ex.getMessage();
                ex.printStackTrace();
            } finally {
                if (conn != null) {
                    // closes the database connection
                    try {
                        conn.close();
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
            }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//==========
		String baseURI=request.getRequestURL().toString();
		baseURI=baseURI.substring(0, baseURI.lastIndexOf('/')+1);
		String apiLocation=baseURI+"carsystemi/Car/addNewCar";
		 URL url = new URL(apiLocation);
		
		String carNumber=request.getParameter("carNumber");
		String type=request.getParameter("type");
		String model=request.getParameter("model");
		String style=request.getParameter("style");
		String color=request.getParameter("color");
		String carDriverNationalId=request.getParameter("carDriverNationalId_addCar");
		String countKm=request.getParameter("countKm");
		String monthlyInstallment=request.getParameter("monthlyInstallment");
		String totalLoan=request.getParameter("totalLoan");
		String remainLoan=request.getParameter("remainLoan");
		String firstInstallmentDate=request.getParameter("firstInstallmentDate");
		String carStatus=request.getParameter("carStatus");
		String paymentType=request.getParameter("paymentType");
		String carCompanyName=request.getParameter("companyName_addCar");
		//String imageUrl=request.getParameter("imageUrl");
		String totalFines=request.getParameter("totalFines");
 
		/*if(carDriverNationalId==null)
			response.getWriter().print("null condtion \n");*/
//		if(carDriverNationalId.length() ==0){
//			carDriverNationalId = "-1";
//			//response.getWriter().print(" length 0 condtion \n");
//		}
		
		//response.getWriter().print("\nDate= " + firstInstallmentDate);
		/*response.getWriter().print(" " + color + " " + countKm + " " + monthlyInstallment + " " + totalFines + " " + 
		remainLoan + " " + firstInstallmentDate + " " + carStatus + " " + paymentType+ " " + imageUrl + " " + totalFines + " ");
		response.getWriter().print(carNumber +" " + type + " " + model + " " + style +" " + carDriverNationalId +" CAR Company NAME="+ carCompanyName);
	
		/********************/
		InputStream inputStream = null; // input stream of the upload file
        
        // obtains the upload file part in this multipart request
        Part filePart = request.getPart("photo");
        response.getWriter().print("===== : "  + filePart);
        if (filePart != null) {
            // prints out some information for debugging
            System.out.println(filePart.getName());
            System.out.println(filePart.getSize());
            System.out.println(filePart.getContentType());
             
            // obtains input stream of the upload file
            inputStream = filePart.getInputStream();
        }
		/*******************/
	
		 Map<String,String> params = new HashMap<String, String>();
		 params.put("carNumber", carNumber);
		 params.put("type", type);
		 params.put("model", model);
		 params.put("style", style);
		 params.put("color", color);
		 params.put("carDriverNationalId", carDriverNationalId);
		 params.put("countKm", countKm);
		 params.put("monthlyInstallment", monthlyInstallment);
		 params.put("totalLoan", totalLoan);
		 params.put("remainLoan", remainLoan);
		 params.put("firstInstallmentDate", firstInstallmentDate);
		 params.put("carStatus", carStatus);
		 params.put("paymentType", paymentType);
		 params.put("carCompanyName", carCompanyName);
		 //params.put("imageUrl", imageUrl);
		 params.put("totalFines", totalFines);
		 
		 StringBuilder postData = new StringBuilder();
	        for (Entry<String, String> param : params.entrySet()) {
	            if (postData.length() != 0) postData.append('&');
	            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
	            postData.append('=');
	            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
	        }
	        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
	        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
	        conn.setRequestMethod("POST");
	        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
	        conn.setDoOutput(true);
	        conn.getOutputStream().write(postDataBytes);
	        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

	        StringBuilder sb = new StringBuilder();
	        for (int c; (c = in.read()) >= 0;)
	            sb.append((char)c);
	        String respoe = sb.toString();
	        //response.getWriter().print("*************");
	        
	        insertImage(inputStream, carNumber);
	        
	        
		    JSONParser parser = new JSONParser();
	        JSONObject json;
			try {
				HttpSession session = request.getSession(true);
				
				json = (JSONObject) parser.parse(respoe);
				int result = Integer.parseInt(json.get("result").toString());
				if(result!=-1){
		        	CarModel obj = new CarModel();
		        	EmployeeModel emp = new EmployeeModel();
		        	ArrayList<EmployeeModel> employees = new ArrayList<EmployeeModel>();
		        	ArrayList<CarModel> allCars = new ArrayList<CarModel>();
		        	try {
						allCars = obj.getAllCars();
						employees = emp.getAllEmployee();
						session.setAttribute("allCars", allCars);
						session.setAttribute("allEmployees", employees);
						response.sendRedirect("homePage.jsp");
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else{
					session.setAttribute("messeg", "This Car Already exist, or No Driver/Company Name With This Info!!!<br> Please Check It again...");
					response.sendRedirect("errorMessege.jsp");
				}
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
	        
	        //response.getWriter().print("jjjjjjjjjjjj"+respoe);
//        response.sendRedirect("keyhome.html");

	//==========
	}

}
