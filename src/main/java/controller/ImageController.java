package controller; 

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import model.DB;

/** 
 * Servlet implementation class ImageController
 */
@WebServlet("/ImageController")
@MultipartConfig(maxFileSize = 16177215)    // upload file's size up to 16MB
public class ImageController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ImageController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
                InputStream inputStream = null; // input stream of the upload file
         
        // obtains the upload file part in this multipart request
        Part filePart = request.getPart("photo");
        response.getWriter().print("===== : "  + filePart);
        if (filePart != null) {
            // prints out some information for debugging
            System.out.println(filePart.getName());
            System.out.println(filePart.getSize());
            System.out.println(filePart.getContentType());
             
            // obtains input stream of the upload file
            inputStream = filePart.getInputStream();
        }
         
        Connection conn = null; // connection to the database
        DB obj = new DB();
        String message = null;  // message will be sent back to client
         
        try {
            // connects to the database
           // DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            conn = obj.dbConnect();
 
            // constructs SQL statement
            String sql = "INSERT INTO carImage values (?,?)";
            PreparedStatement statement;
                    try {
                        statement = conn.prepareStatement(sql);
                        //statement.setString(1, "try");
                        statement.setInt(2, 2);
                        if (inputStream != null) {
                            statement.setBinaryStream(1, inputStream,1000000);
                        }
                        int row = statement.executeUpdate();
                        if (row > 0) {
                            message = "File uploaded and saved into database";
                            response.getWriter().print(message);
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(ImageController.class.getName()).log(Level.SEVERE, null, ex);
                    }
         
             }catch (Exception ex) {
                message = "ERROR: " + ex.getMessage();
                ex.printStackTrace();
            } finally {
                if (conn != null) {
                    // closes the database connection
                    try {
                        conn.close();
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
            }
	}
}
