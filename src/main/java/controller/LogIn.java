package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.runners.AllTests;

import model.CarModel;
import model.CompanyTypeModel;
import model.EmployeeModel;
import model.FinanceCompanyModel;

/**
 * Servlet implementation class LogIn
 */
@WebServlet("/LogIn")
public class LogIn extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogIn() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// ===============================
			String baseURI=request.getRequestURL().toString();
			baseURI=baseURI.substring(0, baseURI.lastIndexOf('/')+1);
			String apiLocation=baseURI+"carsystemi/Employee/logIn";
			 URL url = new URL(apiLocation);
			String nationalId=request.getParameter("nationalId");
			String pass=request.getParameter("password");
			
			if(nationalId.length()==0 || pass.length()==0)
				response.sendRedirect("login.jsp");
	
			else {
				Map<String,String> params = new HashMap<String, String>();
			
				 params.put("nationalId", nationalId);
				 params.put("password", pass);
				  StringBuilder postData = new StringBuilder();
			        for (Entry<String, String> param : params.entrySet()) {
			            if (postData.length() != 0) postData.append('&');
			            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
			            postData.append('=');
			            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
			        }
			        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
			        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			        conn.setRequestMethod("POST");
			        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
			        conn.setDoOutput(true);
			        conn.getOutputStream().write(postDataBytes);
			        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
		
			        StringBuilder sb = new StringBuilder();
			        for (int c; (c = in.read()) >= 0;)
			            sb.append((char)c);
			        String respoe = sb.toString();
			        
			        JSONParser parser = new JSONParser(); 
			        try {
						JSONObject json = (JSONObject) parser.parse(respoe);
						//response.getWriter().print("json parser value reult = " + json.get("result").toString());
						
						int return_result = Integer.parseInt(json.get("result").toString());
						if(return_result==-1)
							response.sendRedirect("login.jsp");
						else {
							HttpSession session = request.getSession(true);
				        	session.setAttribute("active_user", respoe);
				        	CarModel obj = new CarModel();
				        	EmployeeModel emp = new EmployeeModel();
				        	FinanceCompanyModel comanyName = new FinanceCompanyModel();
				        	CompanyTypeModel com_type = new CompanyTypeModel();
				        	
				        	
				        	ArrayList<EmployeeModel> employees = new ArrayList<EmployeeModel>();
				        	ArrayList<FinanceCompanyModel> companies = new ArrayList<FinanceCompanyModel>();
				        	ArrayList<CarModel> allCars = new ArrayList<CarModel>();
				        	ArrayList<CompanyTypeModel> allTypes = new ArrayList<CompanyTypeModel>();
				        	//response.getWriter().print("\n before call Alls \n");
								try {
									allCars = obj.getAllCars();
									//response.getWriter().print("\n after call AllCars \n");
									employees = emp.getAllEmployee();
									//response.getWriter().print("\n after call AllEmployees \n");
									companies = comanyName.getAllFinanceCompanies();
									//response.getWriter().print("\n after call AllCompanyNames \n");
									allTypes = com_type.getAllTypes();
									//response.getWriter().print("\n after call AllTypes \n");
									 
									//response.getWriter().print("\n after call Alls \n");
									session.setAttribute("allCars", allCars);
									session.setAttribute("allEmployees", employees);
									session.setAttribute("allCompanies", companies);
									session.setAttribute("allTypes", allTypes);
									
									
						        	response.sendRedirect("homePage.jsp");
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}	
				        	}
						}
					catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			        
			        /*if(respoe.equals("-1"))
			        	response.sendRedirect("login.jsp");
			        else{
			        	HttpSession session = request.getSession(true);
			        	session.setAttribute("active_user", respoe);
			        	CarModel obj = new CarModel();
			        	ArrayList<CarModel> allCars = new ArrayList<CarModel>();
							try {
								allCars = obj.getAllCars();
								session.setAttribute("allCars", allCars);
					        	response.sendRedirect("homePage.jsp");
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
			        }*/
			        	
			        //
			}
		// ===============================
	}

}
