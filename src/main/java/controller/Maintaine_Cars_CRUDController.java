package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.EmployeeModel;
import model.MaintainanceModel;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

/**
 * Servlet implementation class Maintaine_Cars_CRUDController
 */
@WebServlet("/Maintaine_Cars_CRUDController")
public class Maintaine_Cars_CRUDController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private MaintainanceModel obj;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Maintaine_Cars_CRUDController() {
        super();
        obj=new MaintainanceModel();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("action")!=null){
			List<MaintainanceModel> lstMaintain=new ArrayList<MaintainanceModel>();
			String action=(String)request.getParameter("action");
			Gson gson = new Gson();
			response.setContentType("application/json");
			
			if(action.equals("list")){
				/*int startPageIndex = Integer.parseInt(request.getParameter("jtStartIndex"));
				int numRecordsPerPage = Integer.parseInt(request.getParameter("jtPageSize"));
				try {
					lstUser=obj.getAllEmployee(startPageIndex,numRecordsPerPage);
					int employeeCount = obj.getEmployeeCount();
					//Convert Java Object to Json			
					JsonElement element = gson.toJsonTree(lstUser, new TypeToken<List<EmployeeModel>>() {}.getType());
					JsonArray jsonArray = element.getAsJsonArray();
					String listData=jsonArray.toString();				
					//Return Json in the format required by jTable plugin
					listData="{\"Result\":\"OK\",\"Records\":"+listData+",\"TotalRecordCount\":"+employeeCount+"}";			
					response.getWriter().print(listData);
					}catch(Exception ex){
						String error="{\"Result\":\"ERROR\",\"Message\":"+ex.getMessage()+"}";
						response.getWriter().print(error);
						ex.printStackTrace();
					}
			
				*/try{						
				//Fetch Data from User Table
				lstMaintain=obj.getAllCarsMaintain();		
				//Convert Java Object to Json				
				JsonElement element = gson.toJsonTree(lstMaintain, new TypeToken<List<MaintainanceModel>>() {}.getType());
				JsonArray jsonArray = element.getAsJsonArray();
				String listData=jsonArray.toString();				
				//Return Json in the format required by jTable plugin
				listData="{\"Result\":\"OK\",\"Records\":"+listData+"}";			
				response.getWriter().print(listData);
				}catch(Exception ex){
					String error="{\"Result\":\"ERROR\",\"Message\":"+ex.getMessage()+"}";
					response.getWriter().print(error);
					ex.printStackTrace();
				}			
			}
			else if(action.equals("create") || action.equals("update")){
				MaintainanceModel maintain=new MaintainanceModel();
				if(request.getParameter("serviceCost")!=null){				   
				   double serviceCost=Double.parseDouble(request.getParameter("serviceCost"));
				   maintain.setServiceCost(serviceCost);
				}
				if(request.getParameter("nextServiceDate")!=null){
					String nextServiceDate=(request.getParameter("nextServiceDate"));
					maintain.setNextServiceDate(nextServiceDate);
				}
				if(request.getParameter("serviceDate")!=null){
				   String serviceDate=(String)request.getParameter("serviceDate");
				   maintain.setServiceDate(serviceDate);
				}
				if(request.getParameter("serviceType")!=null){
				   String serviceType=(String)request.getParameter("serviceType");
				   maintain.setServiceType(serviceType);
				}
		
				if(request.getParameter("companyName")!=null){
					   String companyName=(String)request.getParameter("companyName");
					   maintain.setCompanyName(companyName);
					}
					if(request.getParameter("carNumber")!=null){
					   String carNumber=(String)request.getParameter("carNumber");
					   maintain.setCarNumber(carNumber);
					}

				try{											
					if(action.equals("create")){//Create new record
						int result=obj.addNewMaintainance(maintain);
						if(result==0)
							response.getWriter().print("Car Not Found!");
						else {
							lstMaintain.add(maintain);
							//Convert Java Object to Json				
							String json=gson.toJson(maintain);					
							//Return Json in the format required by jTable plugin
							String listData="{\"Result\":\"OK\",\"Record\":"+json+"}";											
							response.getWriter().print(listData);
						}
					}else if(action.equals("update")){//Update existing record
						obj.updateCarMaintaine(maintain);
						//System.out.print("*************");
						String listData="{\"Result\":\"OK\"}";									
						response.getWriter().print(listData);
					}
				}catch(Exception ex){
						String error="{\"Result\":\"ERROR\",\"Message\":"+ex.getStackTrace().toString()+"}";
						response.getWriter().print(error);
				}
			}else if(action.equals("delete")){//Delete record
				try{
					if(request.getParameter("carNumber")!=null){
						String carNumber=(String)request.getParameter("carNumber");
						obj.deleteMaintain(carNumber);
						String listData="{\"Result\":\"OK\"}";								
						response.getWriter().print(listData);
					}
				}catch(Exception ex){
				String error="{\"Result\":\"ERROR\",\"Message\":"+ex.getStackTrace().toString()+"}";
				response.getWriter().print(error);
			}				
		}
	 }
  }

}
