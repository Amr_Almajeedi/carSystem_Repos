package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.CarModel;
import model.CompanyTypeModel;
import model.EmployeeModel;
import model.FinanceCompanyModel;

/**
 * Servlet implementation class UnAssignDriverCar
 */
@WebServlet("/UnAssignDriverCar")
public class UnAssignDriverCar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UnAssignDriverCar() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		CarModel car = new CarModel();
		try {
			car.un_assignDriver_Car(request.getParameter("carNumber"));


			HttpSession session = request.getSession(true);
        	CarModel obj = new CarModel();
        	EmployeeModel emp = new EmployeeModel();
        	FinanceCompanyModel comanyName = new FinanceCompanyModel();
        	CompanyTypeModel com_type = new CompanyTypeModel();
        	
        	
        	ArrayList<EmployeeModel> employees = new ArrayList<EmployeeModel>();
        	ArrayList<FinanceCompanyModel> companies = new ArrayList<FinanceCompanyModel>();
        	ArrayList<CarModel> allCars = new ArrayList<CarModel>();
        	ArrayList<CompanyTypeModel> allTypes = new ArrayList<CompanyTypeModel>();
        	//response.getWriter().print("\n before call Alls \n");
				try {
					allCars = obj.getAllCars();
					//response.getWriter().print("\n after call AllCars \n");
					employees = emp.getAllEmployee();
					//response.getWriter().print("\n after call AllEmployees \n");
					companies = comanyName.getAllFinanceCompanies();
					//response.getWriter().print("\n after call AllCompanyNames \n");
					allTypes = com_type.getAllTypes();
					//response.getWriter().print("\n after call AllTypes \n");
					 
					//response.getWriter().print("\n after call Alls \n");
					session.setAttribute("allCars", allCars);
					session.setAttribute("allEmployees", employees);
					session.setAttribute("allCompanies", companies);
					session.setAttribute("allTypes", allTypes);
					
					
		        	response.sendRedirect("homePage.jsp");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
