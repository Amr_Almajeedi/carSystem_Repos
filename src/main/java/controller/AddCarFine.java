package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.CarModel;

/**
 * Servlet implementation class AddCarFine
 */
@WebServlet("/AddCarFine")
public class AddCarFine extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddCarFine() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//==========
		String baseURI=request.getRequestURL().toString();
		baseURI=baseURI.substring(0, baseURI.lastIndexOf('/')+1);
		String apiLocation=baseURI+"carsystemi/Fine/addNewFine";
		 URL url = new URL(apiLocation);
		 
		
		String carNumber=request.getParameter("carNumber");
		String description=request.getParameter("description");
		String driverNationalId=request.getParameter("driverNationalId");
		String fineAmount=request.getParameter("fineAmount");
		String fineDate=request.getParameter("fineDate");
		String fineType=request.getParameter("fineType");
		
		 Map<String,String> params = new HashMap<String, String>();
		 params.put("fineType", fineType);
		 params.put("description", description);
		 params.put("carNumber", carNumber);
		 params.put("driverNationalId", driverNationalId);
		 params.put("fineAmount", fineAmount);
		 params.put("fineDate", fineDate);
		  
		 
		 /*response.getWriter().print(params.get("fineType") + " , "+ params.get("description") + 
				 " , CC= " + params.get("carNumber") + " , "+ params.get("driverNationalId")
				 + " , " + params.get("fineAmount")+ " , " + params.get("fineDate") );
		*/ 
		  StringBuilder postData = new StringBuilder();
	        for (Entry<String, String> param : params.entrySet()) {
	            if (postData.length() != 0) postData.append('&');
	            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
	            postData.append('=');
	            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
	        }
	        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
	        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
	        conn.setRequestMethod("POST");
	        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
	        conn.setDoOutput(true);
	        conn.getOutputStream().write(postDataBytes);
	        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

	        StringBuilder sb = new StringBuilder();
	        for (int c; (c = in.read()) >= 0;)
	            sb.append((char)c);
	        String respoe = sb.toString();
	        
	        // show message that added successfully
	        response.sendRedirect("homePage.jsp");
	        
	        //response.getWriter().print("jjjjjjjjjjjj"+respoe);
//        response.sendRedirect("keyhome.html");

	//==========
	}

}
