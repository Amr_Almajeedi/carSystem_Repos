package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import model.CarModel;

/**
 * Servlet implementation class DeleteCar
 */
@WebServlet("/DeleteCar")
public class DeleteCar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteCar() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// ===============================
					String baseURI=request.getRequestURL().toString();
					baseURI=baseURI.substring(0, baseURI.lastIndexOf('/')+1);
					String apiLocation=baseURI+"carsystemi/Car/deleteCar";
					 URL url = new URL(apiLocation);
					String carNumber=request.getParameter("carNumber");
					
			
					 Map<String,String> params = new HashMap<String, String>();
					 params.put("carNumber", carNumber);
//					response.getWriter().print("****="+carNumber);
							
					  StringBuilder postData = new StringBuilder();
				        for (Entry<String, String> param : params.entrySet()) {
				            if (postData.length() != 0) postData.append('&');
				            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
				            postData.append('=');
				            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
				        }
				        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
				        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
				        conn.setRequestMethod("POST");
				        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
				        conn.setDoOutput(true);
				        conn.getOutputStream().write(postDataBytes);
				        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			
				        StringBuilder sb = new StringBuilder();
				        for (int c; (c = in.read()) >= 0;)
				            sb.append((char)c);
				        String respoe = sb.toString();
				        JSONParser parser = new JSONParser();
						try {
							HttpSession session = request.getSession(true);
							JSONObject json = (JSONObject) parser.parse(respoe);
							int result = Integer.parseInt(json.get("result").toString());
							if(result!=-1){
								
								CarModel obj = new CarModel();
					        	ArrayList<CarModel> allCars = new ArrayList<CarModel>();
									try {
										allCars = obj.getAllCars();
										session.setAttribute("allCars", allCars);
							        	response.sendRedirect("homePage.jsp");
									} catch (SQLException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}	
					        	}
							else{
								session.setAttribute("messeg", "This Car Not Found!!!<br> Please Check It again...");
								response.sendRedirect("errorMessege.jsp");
							}
								
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
							
				        response.getWriter().print("******="+respoe);
	                	//response.sendRedirect("homePage.jsp");
								
				// ===============================
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
