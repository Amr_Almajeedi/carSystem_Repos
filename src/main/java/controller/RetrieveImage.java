package controller;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.CarModel;
import model.DB;

/**
 * Servlet implementation class RetrieveImage
 */
@WebServlet("/RetrieveImage")
public class RetrieveImage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RetrieveImage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String imageFileName = "car1.jpg";
		
		String contentType = this.getServletContext().getMimeType(imageFileName);
		
		 System.out.println("Content Type: "+ contentType); 
	        response.reset();
         response.setHeader("Content-Type", contentType);
         
         //DB db =new DB();
         CarModel car = new CarModel();
           String id = request.getParameter("id");
          // response.getWriter().print("**********" + id); 
         response.setHeader("Content-Length", String.valueOf(car.getImageData(id).length));
         
         response.setHeader("Content-Disposition", "inline; filename=\"" + "car1.jpg" + "\"");
          
        
         
         response.getOutputStream().write(car.getImageData(id)); 
        response.getOutputStream().flush();  
	}
  
	/**  
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
