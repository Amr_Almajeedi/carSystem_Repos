package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import services.Car;
import model.CompanyTypeModel;
import model.FinanceCompanyModel;

/**
 * Servlet implementation class financeCompany
 */
@WebServlet("/financeCompany")
public class financeCompany extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public financeCompany() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	public boolean insertCompanyName(String companyName,String numOfcar) throws NumberFormatException, SQLException{
		FinanceCompanyModel obj = new FinanceCompanyModel();
		int result = obj.addNewFinanceCompany(companyName, Integer.parseInt(numOfcar));
		
		if(result!=0)
			return true;
		else 
			return false;
	}
	
	public void insertAllTypes(String companyName, String []types, String []typesDescription) throws SQLException{
		CompanyTypeModel obj = new CompanyTypeModel();
		for(int i=0; i<types.length; i++){
			//if(types[i].length()>0)
				obj.addNewCompanyType(types[i], companyName, typesDescription[i]);
		}
	} 

	public void addNewFainanaceCompanyName(HttpServletRequest request, HttpServletResponse response, String companyName) throws IOException{
		String [] types = request.getParameterValues("carType");
		String [] typesDescription = request.getParameterValues("typeDescription");
		String numOfcar = request.getParameter("numOfcar");
		HttpSession session = request.getSession(true);
		try {

			boolean result = insertCompanyName(companyName,numOfcar);
			if(!result){
				session.setAttribute("messeg", "This Company already exist!!!<br> Please Check It again...");
				response.sendRedirect("errorMessege.jsp");
			}
			else {
				insertAllTypes(companyName, types, typesDescription);
				response.sendRedirect("finaneCompany.jsp");
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block  
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
	}
	/** 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String companyName = request.getParameter("companyName_new_field");
		String companyName2 = request.getParameter("companyName_exist_finanace");
		String [] types = request.getParameterValues("carType");
		String [] typesDescription = request.getParameterValues("typeDescription");
		
//		response.getWriter().print("select0" + companyName);
//		response.getWriter().print("new0" + companyName2);
//		
		if(companyName.length()==0){ //add only type
			//response.getWriter().print("select" + companyName);
			try {
				insertAllTypes(companyName2, types, typesDescription);
				response.sendRedirect("finaneCompany.jsp");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//response.sendRedirect("finaneCompany.jsp");
		}
		else{// add new fainanace
			//response.getWriter().print("new " + companyName2);
			addNewFainanaceCompanyName(request,response,companyName);
		}
		
		
		
//		for (String str : types) { 
//			response.getWriter().print("\ncarType: " + str);
//		} 
//		response.getWriter().print("\n*****************\n");
//		for (String str : typesDescription) {
//			response.getWriter().print("\ncarType: " + str);   
//		}
	}

}
