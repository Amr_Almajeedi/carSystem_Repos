package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import model.CarModel;
import model.EmployeeModel;

/**
 * Servlet implementation class CarPage
 */
@WebServlet("/CarPage")
public class CarPage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CarPage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// ===============================
		String baseURI=request.getRequestURL().toString();
		baseURI=baseURI.substring(0, baseURI.lastIndexOf('/')+1);
		String apiLocation=baseURI+"carsystemi/Car/carReport";
		 URL url = new URL(apiLocation);
		String carNumber=request.getParameter("carNumber");

		 Map<String,String> params = new HashMap<String, String>();
		 params.put("carNumber", carNumber);
		  StringBuilder postData = new StringBuilder();
	        for (Entry<String, String> param : params.entrySet()) {
	            if (postData.length() != 0) postData.append('&');
	            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
	            postData.append('=');
	            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
	        }
	        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
	        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
	        conn.setRequestMethod("POST");
	        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
	        conn.setDoOutput(true);
	        conn.getOutputStream().write(postDataBytes);
	        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

	        StringBuilder sb = new StringBuilder();
	        for (int c; (c = in.read()) >= 0;)
	            sb.append((char)c);
	        String respoe = sb.toString();
	        
	        //response.getWriter().print(respoe);
	        JSONParser parser = new JSONParser();
			JSONObject json;
			try {
				HttpSession session = request.getSession(true);
				
				json = (JSONObject) parser.parse(respoe);
				/*CarModel carReport = (CarModel) json.get("carReport");
				EmployeeModel driverReport = (EmployeeModel) json.get("driverReport");*/
				
				
				CarModel carReport = new CarModel();
				EmployeeModel driverReport = new EmployeeModel();
				
				if(Integer.parseInt((json.get("result").toString()))!=-1){
					carReport.setCarNumber(json.get("carReport_carNumber").toString());
					carReport.setCarDriverNationalId(Integer.parseInt(json.get("carReport_carDriverNationalId").toString()));
					carReport.setCountKm(Double.parseDouble(json.get("carReport_countKm").toString()));
					carReport.setCarCompanyName(json.get("carReport_carCompanyName").toString());
					carReport.setModel(json.get("carReport_model").toString());
					carReport.setType(json.get("carReport_type_").toString());
					carReport.setStyle(json.get("carReport_style").toString());
					carReport.setCarStatus(json.get("carReport_carStatus").toString());
					carReport.setPaymentType(json.get("carReport_paymentType").toString());
					carReport.setRemainLoan(Double.parseDouble(json.get("carReport_remainLoan").toString()));
					carReport.setTotalLoan(Double.parseDouble(json.get("carReport_totalLoan").toString()));
					carReport.setFirstInstallmentDate(json.get("carReport_firstInstallmentDate").toString());
					carReport.setMonthlyInstallment(Double.parseDouble(json.get("carReport_monthlyInstallment").toString()));
					carReport.setImageUrl(json.get("carReport_image").toString());
					carReport.setColor(json.get("carReport_color").toString());
					carReport.setTotalFines(Double.parseDouble(json.get("carReport_totalFines").toString()));
					
					session.setAttribute("carReport", carReport);
					session.setAttribute("driverReport", driverReport);
					
					response.sendRedirect("carReportPage.jsp");
				}
				else{
					session.setAttribute("messeg", "");
					response.sendRedirect("error.Messege.jsp");
				}
				/*if(carReport!=null){
					HttpSession session = request.getSession(true);
					session.setAttribute("carReport", carReport);
					session.setAttribute("driverReport", driverReport);
					response.getWriter().print("correct");
					//response.sendRedirect("carReportPage.jsp");
				}else
					response.getWriter().print("null carReport");
				//response.sendRedirect("errorMessege.jsp");
				
*/				} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/**/
	        /*
	         * convert string to carModel object ... 
	         * put it into session
	         * redirect to page to show the object
	         * */
	        	
	        //
	// ===============================
	}

}
