package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import model.CarModel;
import model.CompanyTypeModel;
import model.EmployeeModel;
import model.FinanceCompanyModel;

/**
 * Servlet implementation class Employee_CRUDController
 */
@WebServlet("/Employee_CRUDController")
public class Employee_CRUDController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private EmployeeModel obj;
    
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Employee_CRUDController() {
        super();
        obj=new EmployeeModel();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("action")!=null){
			List<EmployeeModel> lstUser=new ArrayList<EmployeeModel>();
			String action=(String)request.getParameter("action");
			Gson gson = new Gson();
			response.setContentType("application/json");
			
			if(action.equals("list")){
				/*int startPageIndex = Integer.parseInt(request.getParameter("jtStartIndex"));
				int numRecordsPerPage = Integer.parseInt(request.getParameter("jtPageSize"));
				try {
					lstUser=obj.getAllEmployee(startPageIndex,numRecordsPerPage);
					int employeeCount = obj.getEmployeeCount();
					//Convert Java Object to Json			
					JsonElement element = gson.toJsonTree(lstUser, new TypeToken<List<EmployeeModel>>() {}.getType());
					JsonArray jsonArray = element.getAsJsonArray();
					String listData=jsonArray.toString();				
					//Return Json in the format required by jTable plugin
					listData="{\"Result\":\"OK\",\"Records\":"+listData+",\"TotalRecordCount\":"+employeeCount+"}";			
					response.getWriter().print(listData);
					}catch(Exception ex){
						String error="{\"Result\":\"ERROR\",\"Message\":"+ex.getMessage()+"}";
						response.getWriter().print(error);
						ex.printStackTrace();
					}
			
				*/try{						
				//Fetch Data from User Table
				lstUser=obj.getAllEmployee_II();		
				//Convert Java Object to Json				
				JsonElement element = gson.toJsonTree(lstUser, new TypeToken<List<EmployeeModel>>() {}.getType());
				JsonArray jsonArray = element.getAsJsonArray();
				String listData=jsonArray.toString();				
				//Return Json in the format required by jTable plugin
				listData="{\"Result\":\"OK\",\"Records\":"+listData+"}";			
				response.getWriter().print(listData);
				}catch(Exception ex){
					String error="{\"Result\":\"ERROR\",\"Message\":"+ex.getMessage()+"}";
					response.getWriter().print(error);
					ex.printStackTrace();
				}			
			}
		
			
			else if(action.equals("create") || action.equals("update")){
				EmployeeModel emp=new EmployeeModel();
				if(request.getParameter("nationalId")!=null){				   
				   int nationalId=Integer.parseInt(request.getParameter("nationalId"));
				   emp.setNationalId(nationalId);
				}
				if(request.getParameter("employeeNumber")!=null){
					int employeeNumber=Integer.parseInt(request.getParameter("employeeNumber"));
					emp.setEmployeeNumber(employeeNumber);
				}
				if(request.getParameter("name")!=null){
				   String name=(String)request.getParameter("name");
				   emp.setName(name);
				}
				if(request.getParameter("nationality")!=null){
				   String nationality=(String)request.getParameter("nationality");
				   emp.setNationality(nationality);
				}
		
				if(request.getParameter("gender")!=null){
					   String gender=(String)request.getParameter("gender");
					   emp.setGender(gender);
					}
					if(request.getParameter("birthDate")!=null){
					   String birthDate=(String)request.getParameter("birthDate");
					   emp.setBirthDate(birthDate);
					}

					
					if(request.getParameter("mobilePhone")!=null){
						   String mobilePhone=(String)request.getParameter("mobilePhone");
						   emp.setMobilePhone(mobilePhone);
						}
						if(request.getParameter("salary")!=null){
						   double salary=Double.parseDouble(request.getParameter("salary"));
						   emp.setSalary(salary);
						}
				
						if(request.getParameter("jobTitle")!=null){
							   String jobTitle=(String)request.getParameter("jobTitle");
							   emp.setJobTitle(jobTitle);
							}
							if(request.getParameter("birthDate")!=null){
							   String birthDate=(String)request.getParameter("birthDate");
							   emp.setBirthDate(birthDate);
							}

							if(request.getParameter("hiringDate")!=null){
								   String hiringDate=(String)request.getParameter("hiringDate");
								   emp.setHiringDate(hiringDate);
								}
								if(request.getParameter("passportNumber")!=null){
								   String passportNumber=(String)request.getParameter("passportNumber");
								   emp.setPassportNumber(passportNumber);
								}
						
								if(request.getParameter("maritalStatus")!=null){
									   String maritalStatus=(String)request.getParameter("maritalStatus");
									   emp.setMaritalStatus(maritalStatus);
									}
								if(request.getParameter("sponsorName")!=null){
									   String sponsorName=(String)request.getParameter("sponsorName");
									   emp.setSponsorName(sponsorName);
									}
									if(request.getParameter("drivingLicenceNumber")!=null){
									   String drivingLicenceNumber=(String)request.getParameter("drivingLicenceNumber");
									   emp.setDrivingLicenceNumber(drivingLicenceNumber);
									}
									if(request.getParameter("drivingExpireDate")!=null){
										   String drivingExpireDate=(String)request.getParameter("drivingExpireDate");
										   emp.setDrivingExpireDate(drivingExpireDate);
										}
										if(request.getParameter("houseAllowence")!=null){
										   double houseAllowence=Double.parseDouble(request.getParameter("houseAllowence"));
										   emp.setHouseAllowence(houseAllowence);
										}
								
										if(request.getParameter("careamId")!=null){
											   String careamId=(String)request.getParameter("careamId");
											   emp.setCareamId(careamId);
											}
											if(request.getParameter("uberId")!=null){
											   String uberId=(String)request.getParameter("uberId");
											   emp.setUberId(uberId);
											}
											if(request.getParameter("assigned_not")!=null){
												   String assigned_not=(String)request.getParameter("assigned_not");
												   emp.setassigned_not(Integer.parseInt(assigned_not));
												}
											emp.setstatus_assigned_not("not assigned");
											if(request.getParameter("password")!=null){
												   String password=(String)request.getParameter("password");
												   emp.setPassword(password);
												}
				try{											
					if(action.equals("create")){//Create new record
						obj.addNewEmployee(emp);					
						lstUser.add(emp);
						//Convert Java Object to Json				
						String json=gson.toJson(emp);					
						//Return Json in the format required by jTable plugin
						String listData="{\"Result\":\"OK\",\"Record\":"+json+"}";											
						response.getWriter().print(listData);
					}else if(action.equals("update")){//Update existing record
						obj.updateEmployee(emp);
						System.out.print("*************");
						String listData="{\"Result\":\"OK\"}";									
						response.getWriter().print(listData);
					}
				}catch(Exception ex){
						String error="{\"Result\":\"ERROR\",\"Message\":"+ex.getStackTrace().toString()+"}";
						response.getWriter().print(error);
				}
			}else if(action.equals("delete")){//Delete record
				try{
					if(request.getParameter("nationalId")!=null){
						String nationalId=(String)request.getParameter("nationalId");
						//response.getWriter().print("****before****\n");
						obj.deleteEmployee(Integer.parseInt(nationalId));
						String listData="{\"Result\":\"OK\"}";								
						response.getWriter().print(listData);
					}
				}catch(Exception ex){
				String error="{\"Result\":\"ERROR\",\"Message\":"+ex.getStackTrace().toString()+"}";
				response.getWriter().print(error);
			}				
		}
			HttpSession session = request.getSession(true);
			CarModel obj = new CarModel();
        	EmployeeModel emp = new EmployeeModel();
        	FinanceCompanyModel comanyName = new FinanceCompanyModel();
        	CompanyTypeModel com_type = new CompanyTypeModel();
        	
        	
        	ArrayList<EmployeeModel> employees = new ArrayList<EmployeeModel>();
        	ArrayList<FinanceCompanyModel> companies = new ArrayList<FinanceCompanyModel>();
        	ArrayList<CarModel> allCars = new ArrayList<CarModel>();
        	ArrayList<CompanyTypeModel> allTypes = new ArrayList<CompanyTypeModel>();
        	
        	try {
				allCars = obj.getAllCars();
				//response.getWriter().print("\n after call AllCars \n");
				employees = emp.getAllEmployee();
				//response.getWriter().print("\n after call AllEmployees \n");
				companies = comanyName.getAllFinanceCompanies();
				//response.getWriter().print("\n after call AllCompanyNames \n");
				allTypes = com_type.getAllTypes();
				//response.getWriter().print("\n after call AllTypes \n");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
			//response.getWriter().print("\n after call Alls \n");
			session.setAttribute("allCars", allCars);
			session.setAttribute("allEmployees", employees);
			session.setAttribute("allCompanies", companies);
			session.setAttribute("allTypes", allTypes);
			
			
	 }
  }

}
