<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Car Report</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        
        <style>
            
            body,.container-fluid{margin: 0;padding: 0}
            .navbar{margin: 0}
        </style>
        <!--[if lt IE 9]>
            <script src="html5shiv.min"></script>
        <![endif]-->    
    </head>
    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-toggleable-md navbar-light bg-faded navbar-inverse">
              <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <a class="navbar-brand" href="#">Navbar</a>

              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item active">
                    <a class="nav-link" href="#">Employee <span class="sr-only">(current)</span></a>
                  </li>
                  <!-- <li class="nav-item">
                  	<button type="button"  id="addCarItem" class="nav-link" data-toggle="modal" data-target="#updateCarModal">Add Car</button>
                  </li>-->
                  <li class="nav-item">
                    <a class="nav-link" href="#">Cars Maintenance <span class="sr-only">(current)</span></a>
                  </li>
                  <!-- <li class="nav-item">
                    <a class="nav-link" href="#">Finance Companies <span class="sr-only">(current)</span></a>
                  </li>-->
                </ul>
                <ul class="navbar-nav mr-auto navbar-right">
                    <li class="nav-item">
                    	<form action="LogOut" method="post">
                    		<input type="submit" class="nav-item" id="logoutButton" value="Logout">
                    	</form> 
                    </li>
                </ul>
              </div>
            </nav>
        </div>

        
        <div class="container-fluid">
            <div class="card text-center">
              <div class="card-header">
                car title
              </div>
              <div class="card-body">
                <h4 class="card-title">Special title treatment</h4>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
              </div>
              <div class="card-footer text-muted">
                2 days ago
              </div>
            </div>
        </div>
    </body>
</html>