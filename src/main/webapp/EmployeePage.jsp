<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">        
<meta charset="utf-8"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Employee Page</title>
<!-- Include one of jTable styles. -->
<link href="css/metro/crimson/jtable.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
<style>
	.jtable-input-field-container input{padding:3px; width: 278px}
	div.jtable-main-container > div.jtable-title{background-color: #1F2D3A;}
	div.jtable-main-container > div.jtable-title div.jtable-toolbar span.jtable-toolbar-item.jtable-toolbar-item-hover{background-color: #1e5181;}
	div.jtable-main-container > div.jtable-title div.jtable-toolbar span.jtable-toolbar-item{background-color: #1f2d3a;}
	div.jtable-main-container > table.jtable > thead{background-color: #1e5181;padding:5px 0}
	div.jtable-main-container > table.jtable{border: none}
	.jtable-column-header{padding:5px;border-right: 1px solid #aba5a5;}
	div.jtable-main-container > table.jtable > tbody > tr.jtable-data-row > td{padding: 5px}
	
	.navbar{margin: 0;height: 50px}
	#logoutButton{background: none;border: none;color: white;margin-top: 14px;}
    #logoutButton:hover{color:#EEE}
    #addCarItem{background: none;border: none;padding-top: 15px;}
    body,.container-fluid{margin: 0;padding: 0 !important}
    .container-fluid{background-color: #222222;}
    .navbar-inverse{background-color: #1F2D3A}
    div.jtable-main-container > table.jtable > tbody > tr.jtable-row-even{background-color: #cde0f3;}
    div.jtable-main-container > table.jtable > thead th{font-size: 14px}     
    tbody{font-weight: bolder;}   
    
    .ui-widget-header {background: #1f6499; }
    
    #titile_header{text-align: left;font-size: 20px; font-weight: bold; color: #205b92; letter-spacing: 1.5px; margin: 20px 0;}
	#Edit-gender,#Edit-maritalStatus {width: 100%; padding: 6px; }
</style>
<!-- Include jTable script file. -->
<script src="js/jquery-1.8.2.js" type="text/javascript"></script>
<script src="js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
<script src="js/jquery.jtable.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#PersonTableContainer').jtable({
            title: 'Employee Table',
            //paging: true, //Enable paging
            //pageSize: 5, //Set page size (default: 10)
            //sorting: true, //Enable sorting
            //defaultSorting: 'Name ASC', //Set default sorting
            
            actions: {
                listAction: 'Employee_CRUDController?action=list',
                createAction:'Employee_CRUDController?action=create',
                updateAction: 'Employee_CRUDController?action=update',
                deleteAction: 'Employee_CRUDController?action=delete'
            },
        
            fields: { 
            	nationalId: {
                	title:'National ID',
                    key: true,
                    list: true,
                    edit:false,
                    width: '7%',
                    create:true
                },
                employeeNumber: {
                    title: 'Emp. Number',
                    width: '10%',
                    list: true,
                    create:true,
                    edit:true
                },
                name: {
                    title: 'Name',
                    width: '7%',
                    create:true,
                    list: true,
                    edit:true
                },
                nationality: {
                    title: 'Nationality',
                    width: '10%',
                    create:true,
                    list: true,
                    edit:true
                },
                gender: {
                    title: 'Gender',
                    width: '7%',
                    list: false,
                    create:true,
                    edit: false,
                    options:{"male":"male","female":"female"}
                },
                birthDate: {
                    title: 'Birth Date',
                    width: '5%',
                    list: false,
                    create:true,
                    edit:false,
                    type:"date"
                },
                mobilePhone: {
                    title: 'Mobile Phone',
                    width: '5%',
                    list: false,
                    create:true,
                    edit:true
                },
                salary: {
                    title: 'Salary',
                    width: '7%',
                    list: true,
                    create:true,
                    edit: true
                } ,  
                jobTitle: {
                    title: 'Job Title',
                    width: '10%',
                    list: true,
                    create:true,
                    edit:true
                },
                hiringDate: {
                    title: 'Hiring Date',
                    width: '7%',
                    create:true,
                    edit: false,
                    list:true,
                    type:"date"
                }  
                ,  
                passportNumber: {
                    title: 'Passport Number',
                    width: '10%',
                    create:true,
                    edit:true,
                    list:false
                },
                maritalStatus: {
                    title: 'Marital Status',
                    width: '10%',
                    list: false,
                    create:true,
                    edit: true,
                    options:{"single":"single","married":"married"}
                } ,  
                sponsorName: {
                    title: 'Sponsor Name',
                    width: '10%',
                    list: true,
                    create:true,
                    edit:true
                },
                drivingLicenceNumber: {
                    title: 'Licence Number',
                    width: '10%',
                    list: false,
                    create:true,
                    edit:true
                },
                drivingExpireDate: {
                    title: 'Expire Date',
                    width: '7%',
                    create:true,
                    list: true,
                    edit: true,
                    type:"date"
                },  
                houseAllowence: {
                    title: 'House Allowence',
                    width: '10%',
                    list: true,
                    create:true,
                    edit:true
                },
                careamId: {
                    title: 'Caream ID',
                    width: '10%',
                    create:true,
                    list: true,
                    edit: true
                },  
                uberId: {
                    title: 'Uber ID',
                    width: '10%',
                    list: true,
                    create:true,
                    edit:true
                },
                assigned_not: {
                    title: 'Status',
                    width: '10%',
                    list: false,
                    create:false,
                    edit:false
                },
                status_assigned_not: {
                    title: 'Status',
                    width: '10%',
                    list: true,
                    create:false,
                    edit:false,
                    listClass: 'status_id'
                },
                password: {
                    title: 'Password',
                    width: '20%',
                    create:true,
                    list: false,
                    edit: true,
                    type: "password"
                }
            }
        });
        $('#PersonTableContainer').jtable('load');
    });

</script>
</head>
<body>
<div class="container-fluid">
            <nav class="navbar navbar-toggleable-md navbar-light bg-faded navbar-inverse">
              <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <a class="navbar-brand" href="homePage.jsp">Navbar</a>

              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="EmployeePage.jsp">Employee <span class="sr-only">(current)</span></a>
                  </li>
                  <!-- <li class="nav-item">
                  	<button type="button"  id="addCarItem" class="nav-link" data-toggle="modal" data-target="#AddCarModal">Add Car</button>
                  </li>-->
                  <li class="nav-item">
                    <a class="nav-link" href="carsMaintainance.jsp">Cars Maintenance <span class="sr-only">(current)</span></a>
                  </li>
                   <li class="nav-item">
                    <a class="nav-link" href="finaneCompany.jsp">Finance Companies <span class="sr-only">(current)</span></a>
                  </li>
                </ul> 
                <ul class="navbar-nav mr-auto navbar-right">
                    <li class="nav-item">
                    	<form action="LogOut" method="post">
                    		<input type="submit" class="nav-item" id="logoutButton" value="Logout">
                    	</form> 
                    </li>
                </ul>
              </div>
            </nav>
        </div>
<div style="width:94%;margin-right:3%;margin-left:3%;text-align:center;">
<h1 id="titile_header">Employees Information</h1>
<div id="PersonTableContainer"></div>
<script>
$(document).ready(function(){
    var tdLink = $('.status_id');

    tdLink.click(function(){
         alert('blue-theme');
    });
});

</script>
<!-- ============================================================== -->
        <!-- Start Add Car -->
       		<!-- Modal -->
			<div class="modal fade" id="AddCarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <!-- ***************** -->
			
                        <!--start Add Car-->
					        <form action="AddNewCar" method="post">
							  <div class="form-group">
							  	<label class="form-control-label">Car Number</label>
							    <input class="form-control" name="carNumber" type="text" id="carNumber">
							  </div>
							  <div class="form-group">
							  	<label class="form-control-label">Type</label>
							    <input class="form-control" name="type" type="text" id="Type">
							  </div>
							  <div class="form-group">
							  	<label class="form-control-label">model</label>
							    <input class="form-control" name="model" type="text" id="model">
							  </div>
							  <div class="form-group">
							  	<label class="form-control-label">style</label>
							    <input class="form-control" name="style" type="text" id="style">
							  </div>
							  <div class="form-group">
							  	<label class="form-control-label">color</label>
							    <input class="form-control" name="color" type="text" id="color">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Driver NationalId</label>
							    <input class="form-control" name="carDriverNationalId" type="text"  id="carDriverNationalId">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">CountKm</label>
							    <input class="form-control" name="countKm" type="text" id="countKm">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Monthly Installment</label>
							    <input class="form-control" name="monthlyInstallment" type="text" id="monthlyInstallment">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Total Loan</label>
							    <input class="form-control" name="totalLoan" type="text" id="totalLoan">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">RemainLoan</label>
							    <input class="form-control" name="remainLoan" type="text" id="remainLoan">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">First Installment Date</label>
							    <input class="form-control" type="text" name="firstInstallmentDate" id="firstInstallmentDate">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">CarStatus</label>
							    <input class="form-control" name="carStatus" type="text" id="carStatus">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">PaymentType</label>
							    <input class="form-control" name="paymentType" type="text" id="paymentType">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Car Company Name</label>
							    <input class="form-control" name="carCompanyName" type="text"  id="carCompanyName">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">imageUrl</label>
							    <input class="form-control" name="imageUrl" type="text"  id="imageURL">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Total Fines</label>
							    <input class="form-control" name="totalFines" type="text" name="totalFines" id="totalFines">
							  </div>
							  <div class="modal-footer">
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						        <input type="submit" class="btn btn-primary" value="Save change">
						      </div>
							</form>
						<!--end Add Car-->
                    <!-- ***************** -->
			      </div>
			    </div>
			  </div>
			</div>
        <!-- end Modal -->
        <!-- End Add Car -->
        <!-- ============================================================== -->
</div>
</body>
</html>