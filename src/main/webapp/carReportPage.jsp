<%@page import="model.EmployeeModel"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="model.CarModel" %>
    <%@page import="java.util.ArrayList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <meta charset="utf-8"/>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        
        <title>Car Report</title>
        <style>
            body,.container-fluid{margin: 0;padding: 0 !important}
            .container-fluid{background-color: #222222;}
            .navbar-inverse{background-color: #1F2D3A}
            .navbar{margin: 0;height: 50px}
            .card-deck{margin: 15px}
            .card-deck .card:not(:first-child){margin-left: 0}
            #logoutButton{background: none;border: none;color: white;margin-top: 14px;}
            #logoutButton:hover{color:#EEE}
            #addCarItem{background: none;border: none;padding-top: 15px;}
            #nav-tabContent{width: 600px}
            
            .tab-pane{text-align: left}
            .row{margin-top: 20px}
            h4{text-transform: capitalize;}
            p{display: inline}
           .blockInfo{margin :10px 5px;}
        </style>
        <!--[if lt IE 9]>
            <script src="html5shiv.min"></script>
        <![endif]-->    
    </head>
	    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
        
        <script src="js/bootstrap.js"></script>
        
    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-toggleable-md navbar-light bg-faded navbar-inverse">
              <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <a class="navbar-brand" href="homePage.jsp">Navbar</a>

              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="EmployeePage.jsp">Employee <span class="sr-only">(current)</span></a>
                  </li>
                  <!-- <li class="nav-item">
                  	<button type="button"  id="addCarItem" class="nav-link" data-toggle="modal" data-target="#AddCarModal">Add Car</button>
                  </li>-->
                  <li class="nav-item">
                    <a class="nav-link" href="carsMaintainance.jsp">Cars Maintenance <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="finaneCompany.jsp">Finance Companies <span class="sr-only">(current)</span></a>
                  </li>
                </ul>
                <ul class="navbar-nav mr-auto navbar-right">
                    <li class="nav-item">
                    	<form action="LogOut" method="post">
                    		<input type="submit" class="nav-item" id="logoutButton" value="Logout">
                    	</form> 
                    </li>
                </ul>
              </div>
            </nav>
        </div>
		
		<%  CarModel carReport= (CarModel)session.getAttribute("carReport");
			EmployeeModel driverReport= (EmployeeModel)session.getAttribute("driverReport");
			
			String carStatus = carReport.getCarStatus();
			String style = carReport.getStyle();
			Double fines = carReport.getTotalFines();
			String title_carType= carReport.getType();
			String title_carCompanyName= carReport.getCarCompanyName();
			String title_carModel= carReport.getModel();
			String subTitle_carNumber = carReport.getCarNumber();
			String subTitle_driver = "";
			if(driverReport.getName().equals(null) || driverReport.getName().length()<=0){
				subTitle_driver = "not_yet_assigned,,,";
			}
			else {
				subTitle_driver = driverReport.getName();
			}
			Double subTitle_countKm = carReport.getCountKm();
			String bodyText_carInfo ;
			String date;
			int assigned_not = carReport.getassigned_not();
			
			
			String name = driverReport.getName();
			int nationalId = driverReport.getNationalId();
			String phoneNumber=driverReport.getMobilePhone();
			String careamId=driverReport.getCareamId();
			String uberId=driverReport.getUberId();
			String licence=driverReport.getDrivingLicenceNumber();
			String drivingExpireDate=driverReport.getDrivingExpireDate();
			double salary=driverReport.getSalary();
			
			String First_Installment_Date = carReport.getFirstInstallmentDate();
			double Monthly_Installment= carReport.getMonthlyInstallment();
			String Payment_Type = carReport.getPaymentType();
			double remainLoan =carReport.getRemainLoan();
			double totalLoan = carReport.getTotalLoan();
			String status="";
			if(carReport.getassigned_not()==0){
				status = "Not Assigned";
			}
			else
				status = "Assigned to "+ name;

			%>
        <script>$(function () {
		  $('[data-toggle="popover"]').popover()
		})</script>
		  
		  <!-- car Report -->
		  <div class="container-fluid">
            <div class="card text-center">
              <div class="card-header">
              	<h4><label class="text-muted"><%=title_carCompanyName %></label> ,Model: <%=title_carModel %></h4>
               </div> 
              <div class="card-body">
                <h5 class="card-title">Car Number: <label class="text-muted"><%=subTitle_carNumber %></label>, Driven by: <label class="text-muted"><%=subTitle_driver %></label> about <label class="text-muted"><%=subTitle_countKm %></label>Km.</h5>
                <div class="row">
                  <div class="col-lg-offset-1 col-4">
               		<div class="list-group" id="list-tab" role="tablist">
                      <a class="list-group-item list-group-item-action" id="list-profile-list"    data-toggle="list" href="#car-info" role="tab" aria-controls="profile">Car Information</a>
                      <a class="list-group-item list-group-item-action" id="list-driver-list"       data-toggle="list" href="#driver-info" role="tab" aria-controls="home">Car Driver</a>
                      <a class="list-group-item list-group-item-action" id="list-messages-list"   data-toggle="list" href="#installment" role="tab" aria-controls="messages">Car Installments</a>
                      <a class="list-group-item list-group-item-action" id="list-settings-list"   data-toggle="list" href="#Loan" role="tab" aria-controls="settings">Car Loan</a>
                    </div>
               </div>
              <div class="col-7">
                    <div class="tab-content" id="nav-tabContent">
                       <div class="alert alert-info tab-pane fade" id="car-info" role="tabpanel">
                          <h4 class="alert-heading"><label class="text-muted"><%=title_carType %></label> ,Model: <%=title_carModel %></h4>
                         <div class="blockInfo"> <label class="text-muted">Status: </label><p> <%=carStatus %></p></div>
                          <div class="blockInfo"><label class="text-muted">Style: </label><p> <%=style %></p></div>
                         <div class="blockInfo"> <label class="text-muted">Total Fines: </label><p> <%=fines %></p></div>
                         <div class="blockInfo"> <label class="text-muted">Car Status </label><p> <%=status %></p></div>
                          <hr>
                          <p class="mb-0">write Whatever you want.</p>
                       </div>
                    
	                   <div class="alert alert-info tab-pane fade" id="driver-info" role="tabpanel">
	                         <h4 class="alert-heading"><label class="text-muted"><%=name %></label> ,Phone: <%=phoneNumber %> ,National Id: <%=nationalId%> </h4>
	                       <div class="blockInfo"> <label class="text-muted">careamId: </label><p> <%=careamId %></p></div>
                          <div class="blockInfo"><label class="text-muted">uberId: </label><p> <%=uberId %></p></div>
                         <div class="blockInfo"> <label class="text-muted">drivingLicenceNumber: </label><p> <%=licence %></p></div>
                          <div class="blockInfo"><label class="text-muted">drivingExpireDate: </label><p> <%=drivingExpireDate %></p></div>
                         <div class="blockInfo"> <label class="text-muted">salary: </label><p> <%=salary %></p></div>
	                         <hr>
	                         <p class="mb-0">write Whatever you want.</p>
	                   </div>
	                   
	                   <div class="alert alert-success tab-pane fade" id="installment" role="tabpanel">
	                         <h4 class="alert-heading">Car Installments</h4>
	                         <div class="blockInfo"><label class="text-muted">First Installment Date : </label><p> <%=First_Installment_Date %></p></div>
	                        <div class="blockInfo"> <label class="text-muted">Monthly Installment: </label><p> <%=Monthly_Installment %></p></div>
	                        <div class="blockInfo"> <label class="text-muted">Payment Type: </label><p> <%=Payment_Type %></p></div>
	                         <hr>
	                         <p class="mb-0">write Whatever you want.</p>
	                   </div>
	                   
	                   <div class="alert alert-danger tab-pane fade" id="Loan" role="tabpanel">
	                         <h4 class="alert-heading">Car Loan</h4>
	                         <div class="blockInfo"> <label class="text-muted">totalLoan: </label><p> <%=totalLoan %></p></div>
	                         <div class="blockInfo"><label class="text-muted">remainLoan: </label><p> <%=remainLoan %></p></div>
	                         <hr>
	                         <p class="mb-0">write Whatever you want.</p>
	                   </div>
                    </div>
              </div>
             
                </div>
                <a href="homePage.jsp" class="btn btn-primary">Go to Dashboard</a>
              </div>
              <div class="card-footer text-muted">
                2 days ago
              </div>
            </div>
        </div>
		  <!-- end Car Report -->  
		  
		  <!-- ============================================================== -->
        <!-- Start Add Car -->
       		<!-- Modal -->
			<div class="modal fade" id="AddCarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <!-- ***************** -->
			
                        <!--start Add Car-->
					        <form action="AddNewCar" method="post">
							  <div class="form-group">
							  	<label class="form-control-label">Car Number</label>
							    <input class="form-control" name="carNumber" type="text" id="carNumber">
							  </div>
							  <div class="form-group">
							  	<label class="form-control-label">Type</label>
							    <input class="form-control" name="type" type="text" id="Type">
							  </div>
							  <div class="form-group">
							  	<label class="form-control-label">model</label>
							    <input class="form-control" name="model" type="text" id="model">
							  </div>
							  <div class="form-group">
							  	<label class="form-control-label">style</label>
							    <input class="form-control" name="style" type="text" id="style">
							  </div>
							  <div class="form-group">
							  	<label class="form-control-label">color</label>
							    <input class="form-control" name="color" type="text" id="color">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Driver NationalId</label>
							    <input class="form-control" name="carDriverNationalId" type="text"  id="carDriverNationalId">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">CountKm</label>
							    <input class="form-control" name="countKm" type="text" id="countKm">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Monthly Installment</label>
							    <input class="form-control" name="monthlyInstallment" type="text" id="monthlyInstallment">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Total Loan</label>
							    <input class="form-control" name="totalLoan" type="text" id="totalLoan">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">RemainLoan</label>
							    <input class="form-control" name="remainLoan" type="text" id="remainLoan">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">First Installment Date</label>
							    <input class="form-control" type="text" name="firstInstallmentDate" id="firstInstallmentDate">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">CarStatus</label>
							    <input class="form-control" name="carStatus" type="text" id="carStatus">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">PaymentType</label>
							    <input class="form-control" name="paymentType" type="text" id="paymentType">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Car Company Name</label>
							    <input class="form-control" name="carCompanyName" type="text"  id="carCompanyName">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">imageUrl</label>
							    <input class="form-control" name="imageUrl" type="text"  id="imageURL">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Total Fines</label>
							    <input class="form-control" name="totalFines" type="text" name="totalFines" id="totalFines">
							  </div>
							  <div class="modal-footer">
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						        <input type="submit" class="btn btn-primary" value="Save change">
						      </div>
							</form>
						<!--end Add Car-->
                    <!-- ***************** -->
			      </div>
			    </div>
			  </div>
			</div>
        <!-- end Modal -->
        <!-- End Add Car -->
        <!-- ============================================================== -->
	
		<% if(assigned_not == 0 || (assigned_not+"")==null){ // car has no driver yet !!!
				%><script>
				document.getElementById("list-driver-list").style.display="none";
				</script>
			<%}
			%>
		   
        <!--Footer-->
        
        <!--End Footer-->
    </body>
</html>