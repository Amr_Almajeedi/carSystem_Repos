<%@page import="model.FinanceCompanyModel"%>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">        
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title></title>
        
        <style></style>
        <!--[if lt IE 9]>
            <script src="html5shiv.min"></script>
        <![endif]-->    
    </head>
    
    <style>
        #companyForm input[type="text"]{display: block; margin-bottom: 15px}
        .buto{    display: inline-block;float: left;margin-right: 5px;}
        
        .navbar{margin: 0;height: 50px}
        #logoutButton{background: none;border: none;color: white;margin-top: 14px;}
        #logoutButton:hover{color:#EEE}
        #addCarItem{background: none;border: none;padding-top: 15px;}
        body,.container-fluid{margin: 0;padding: 0 !important}
        .container-fluid{background-color: #222222;}
        .navbar-inverse{background-color: #1F2D3A}

        #addtype{margin: 5% 5%}
        #addnewheader{letter-spacing: 1px; font-size: 16px; font-weight: bold; color: #3e5d7a;}
        label{font-weight: 100; margin-bottom: 10px;}
        #companyNameSelet {padding: 6px 12px; width: 100%; margin-top: 0px;border-radius: 5px;margin-bottom: 0;}
    	#add_new_fainance{border: 1px solid #abbfd2; margin-bottom: 5px;}
    </style>
    
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
     
    <script>
        var count_ = 1 ;
        function addtype(){ 
            count_= count_+1;
            //alert("add type function");
            $("#typeForm_div").append('<hr><div class="col-xs-6"> <div class="form-group" id="form_div_type"><label class="form-control-label">Car Type '+count_+ '</label><input class="form-control" name="carType" type="text" id="carType" required></div><div class="form-group"><label for="exampleTextarea">Type Description</label><textarea class="form-control" id="typeDescription" name="typeDescription" rows="3" required></textarea></div> </div>');
            
            document.getElementById("numOfcar").value=count_;
        } 
        
        function dispay_newFainanace(){
            document.getElementById("companyName_field").style.display= "block";
            document.getElementById("companyNameSelet").style.display= "none";
        }
        
        function getSelectedCompanyName(){
        	var yourSelect = document.getElementById("companyNameSelet" );
        	//alert( yourSelect.options[ yourSelect.selectedIndex ].value );
        	
        	document.getElementById("companyName_addCar").value = yourSelect.options[ yourSelect.selectedIndex ].value;
        }
        
    </script>
    <body>
        
        <div class="container-fluid">
            <nav class="navbar navbar-toggleable-md navbar-light bg-faded navbar-inverse">
              <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <a class="navbar-brand" href="homePage.jsp">Navbar</a>

              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="EmployeePage.jsp">Employee <span class="sr-only">(current)</span></a>
                  </li>
                  <!-- <li class="nav-item">
                  	<button type="button"  id="addCarItem" class="nav-link" data-toggle="modal" data-target="#AddCarModal">Add Car</button>
                  </li>-->
                  <li class="nav-item">
                    <a class="nav-link" href="carsMaintainance.jsp">Cars Maintenance <span class="sr-only">(current)</span></a>
                  </li>
                   <li class="nav-item">
                    <a class="nav-link" href="finaneCompany.jsp">Finance Companies <span class="sr-only">(current)</span></a>
                  </li>
                </ul>
                <ul class="navbar-nav mr-auto navbar-right">
                    <li class="nav-item">
                    	<form action="LogOut" method="post">
                    		<input type="submit" class="nav-item" id="logoutButton" value="Logout">
                    	</form> 
                    </li>
                </ul>
              </div>
            </nav>
        </div>
         <!--****************************************************************-->
         <%
	     	ArrayList<FinanceCompanyModel> companies = (ArrayList<FinanceCompanyModel>) session.getAttribute("allCompanies");
	     	if(companies == null || companies.size()==0){
	     		FinanceCompanyModel obj = new FinanceCompanyModel();
	     		companies = obj.getAllFinanceCompanies();
	     	}
         %>
        <div id="addtype">
             <label class="form-control-label" id="addnewheader">Add New Type</label>
            <form action="financeCompany" id="companyForm" method="post">
                    <div class="row">
                        <div class="form-group col-xs-12">
                        	<a onclick="dispay_newFainanace()" class="btn btn-secondary" id="add_new_fainance" style="float:right">Add New Finance Company</a>
                            <label class="form-control-label" style="display:block">Company Name</label>
                            
                            <select name="companyNameSelet" id="companyNameSelet" style="float:left" onchange="getSelectedCompanyName()">
						    	<%for(int i=0; i<companies.size(); i++){
						    		FinanceCompanyModel objCompany = new FinanceCompanyModel();
						    		objCompany = companies.get(i); %>
						    		<option value=<%=objCompany.getCompanyName() %> class="options" selected="selected"><%=objCompany.getCompanyName() %></option>
						    	<%} %>
                   			</select>

                            <div class="form-group">
							    <input class="form-control" name="companyName_exist_finanace" type="hidden"  id="companyName_addCar">
							 </div>
							
							<input style="display:none" class="form-control" name="companyName_new_field" type="text" id="companyName_field">
                        </div>
                    </div> 
                     
                    <div class="row" id="typeForm_div">
                        <div class="col-xs-6">    
                            <div class="form-group" id="form_div_type">
                                <label class="form-control-label">Car Type</label>
                                <input class="form-control" name="carType" type="text" id="carType" required>
                            </div>

                            <div class="form-group">
                                <label for="exampleTextarea">Type Description</label>
                                <textarea class="form-control" id="typeDescription" name="typeDescription" rows="3" required></textarea>
                            </div>

                        </div>
                    </div>
                <hr>
                <div class="form-group">
                        <label class="form-control-label text-muted">Total Number Of Cars</label>
                        <input class="form-control" name="numOfcar" type="text" id="numOfcar" value="1" readonly>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" onclick="addtype()">+ Add Another Type</button>
                        <input type="submit" class="btn btn-primary" value="Save change">
                    </div>
            </form>
        </div>
        <!--****************************************************************-->
        <!-- ============================================================== -->
        <!-- Start Add Car -->
       		<!-- Modal -->
			<div class="modal fade" id="AddCarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <!-- ***************** -->
			
                        <!--start Add Car-->
					        <form action="AddNewCar" method="post">
							  <div class="form-group">
							  	<label class="form-control-label">Car Number</label>
							    <input class="form-control" name="carNumber" type="text" id="carNumber">
							  </div>
							  <div class="form-group">
							  	<label class="form-control-label">Type</label>
							    <input class="form-control" name="type" type="text" id="Type">
							  </div>
							  <div class="form-group">
							  	<label class="form-control-label">model</label>
							    <input class="form-control" name="model" type="text" id="model">
							  </div>
							  <div class="form-group">
							  	<label class="form-control-label">style</label>
							    <input class="form-control" name="style" type="text" id="style">
							  </div>
							  <div class="form-group">
							  	<label class="form-control-label">color</label>
							    <input class="form-control" name="color" type="text" id="color">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Driver NationalId</label>
							    <input class="form-control" name="carDriverNationalId" type="text"  id="carDriverNationalId">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">CountKm</label>
							    <input class="form-control" name="countKm" type="text" id="countKm">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Monthly Installment</label>
							    <input class="form-control" name="monthlyInstallment" type="text" id="monthlyInstallment">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Total Loan</label>
							    <input class="form-control" name="totalLoan" type="text" id="totalLoan">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">RemainLoan</label>
							    <input class="form-control" name="remainLoan" type="text" id="remainLoan">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">First Installment Date</label>
							    <input class="form-control" type="text" name="firstInstallmentDate" id="firstInstallmentDate">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">CarStatus</label>
							    <input class="form-control" name="carStatus" type="text" id="carStatus">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">PaymentType</label>
							    <input class="form-control" name="paymentType" type="text" id="paymentType">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Car Company Name</label>
							    <input class="form-control" name="carCompanyName" type="text"  id="carCompanyName">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">imageUrl</label>
							    <input class="form-control" name="imageUrl" type="text"  id="imageURL">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Total Fines</label>
							    <input class="form-control" name="totalFines" type="text" name="totalFines" id="totalFines">
							  </div>
							  <div class="modal-footer">
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						        <input type="submit" class="btn btn-primary" value="Save change">
						      </div>
							</form>
						<!--end Add Car-->
                    <!-- ***************** -->
			      </div>
			    </div>
			  </div>
			</div>
        <!-- end Modal -->
        <!-- End Add Car -->
        <!-- ============================================================== -->
    </body>
</html>