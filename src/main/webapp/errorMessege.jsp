<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/bootstrap.css">
        
        <title></title>
        <style>
            body{padding: 0; margin: 0;background: url(images/bodyback.jpg); background-size: cover;background-repeat: no-repeat}
            .container-fluid{text-align: center;padding-top: 50px}
            #error_messge h3{color: white;text-align: center;letter-spacing: 2px;line-height: 32px;}
            #error_messge{text-align: center}
            .btn-outline {background-color: transparent; color: inherit; transition: all .5s; margin: 30px 0;}
        </style>
        <!--[if lt IE 9]>
            <script src="html5shiv.min"></script>
        <![endif]-->    
    </head>
    <body>
    	<% String errorMesseg = session.getAttribute("messeg").toString(); %>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div id="error_messge">
                        <img src="images/error5.png">
                        <h3><em><%=errorMesseg %></em></h3>
                        <a class="btn btn-default btn-outline" href="homePage.jsp">Go Back to Dashboard</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>