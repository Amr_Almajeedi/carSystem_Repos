<%@page import="model.CompanyTypeModel"%>
<%@page import="model.FinanceCompanyModel"%>
<%@page import="model.EmployeeModel"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="model.CarModel" %>
    <%@page import="java.util.ArrayList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <meta charset="utf-8"/>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        
        <title>Dashboard</title>
        <style>
        #type{padding: 5px;width: 350px;margin-top: 0px;margin-bottom: 25px}
        .options{padding: 5px}
        
        h4{text-transform: capitalize;}
            p{display: inline}
           .blockInfo{margin :10px 5px;}
            body,.container-fluid{margin: 0;padding: 0}
            .container-fluid{background-color: #222222;}
            .navbar-inverse{background-color: #1F2D3A}
            .navbar{margin: 0; height: 50px}
            .card-deck{margin: 15px}
            .card-deck .card:not(:first-child){margin-left: 0}
            #logoutButton{background: none;border: none;color: white;margin-top: 14px;}
            #logoutButton:hover{color:#EEE}
            #popUpUpdateCarContainer{text-align: center; border-radius: 4px;background-color: #F0F8FF; display: none;width: 400px;height: 290px;padding: 40px;;position: fixed;z-index: 1;left: 0;top: 0; margin-left: 550px;margin-top: 160px;padding: 20px 20px;z-index: 1;left: 0;top: 0;overflow: auto;}
			#UpdateCar{width: 314px;height: 40px;padding: 10px;margin-bottom: 15px;background-color: #999;border: none;color: white}
			#updateCarHeader{position: relative;top: -10px;}
			#popUpUpdateCarContainer .animate{position: relative;top: -35px}
			#popUpUpdateCarContainer .LognamePass{margin-bottom: 10px;width: 350px}
			#popUpUpdateCarContainer #UpdateCar{margin-top: 25px;width: 360px}
			#popUpUpdateCarContainer .close{color: black;font-size: 20px;display: inline;;position: relative;top: 5px;left: 17px}
            .LognamePass{display: block;width: 300px;margin-bottom: 20px;height: 25px;background-color: none;padding: 5px}
            
            #addCarItem{background: none;border: none;padding-top: 15px;}
            
            /*------------*/
	            body{background-color: #F4F4F4}
	            body,.container-fluid{margin: 0;padding: 0 !important}
	            .navbar{margin: 0}
	            #car_cards{margin-top: 19px;margin-left: 19px;text-align: center;padding: 50px;}
	            .card{margin-bottom: 20px;margin-right: -17px;}
	            .photo {height: 150px;position: relative;}
	            .btn{float: left;margin-bottom: 5px}
	            .card-footer{padding-right: 0}
	            .btn_car{width: 45%; margin-right: 5px}
	            .btn_main_dele{width: 93%}
            /*------------*/
            #overlay {position: absolute;bottom: 100%;left: 0;right: 0;background-color: #b3dcff;opacity: 0.8;overflow: hidden;width: 100%;height:0;transition: .5s ease;}
            /*.container:hover #overlay {bottom: 0;height: 100%;}*/
            .text {color: white;font-size: 20px;position: absolute;overflow: hidden;top: 50%;left: 50%;transform: translate(-50%, -50%);-ms-transform: translate(-50%, -50%);}
            .text button{width: 150px}
            .assinged_notAssigned{border-radius: 20px;font-weight: bold;letter-spacing: 1px;padding: 5px;color: white;border: none;cursor: pointer;margin-top: 5px;width: 155px}
       
            #assign_car{display: none;background-color: #1b065b}
            #assign_car:hover {background-color: #2a9254 !important;box-shadow: 2px 3px 5px #2a9254}
            #assigned_car{display: none;background-color: #a30404}
            #type,#type2,#companyNameSelet {padding: 6px 12px; width: 100%; margin-top: 0px;border-radius: 5px;margin-bottom: 0;}
            .modal-title{position: absolute;} 
            .closeModal{margin-left: 450px;}
            
            #driverID_FormDIV{display: none;margin-top: 13px;}
            #driverID_FormDIV2{margin-top: 13px}
            /***********checkBox ****************/
            .checkbox-primary input[type="checkbox"]:checked + label::before {background-color: #428bca;border-color: #428bca; }
			.checkbox-primary input[type="checkbox"]:checked + label::after {color: #fff; }
			.checkbox {padding-left: 20px; }
  			.checkbox label {display: inline-block;position: relative;padding-left: 5px; color:#9b8787;letter-spacing: 1px; }
    		.checkbox label::before {content: "";display: inline-block;position: absolute;width: 17px;height: 17px;left: 0;margin-left: -20px;border: 1px solid #cccccc;border-radius: 3px;background-color: #fff;-webkit-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;-o-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;transition: border 0.15s ease-in-out, color 0.15s ease-in-out; }
    		.checkbox label::after {display: inline-block;position: absolute;width: 16px;height: 16px;left: 0;top: 0;margin-left: -20px;padding-left: 3px;padding-top: 1px;font-size: 11px;color: #555555; }
  			.checkbox input[type="checkbox"] {opacity: 0; }
    		.checkbox input[type="checkbox"]:focus + label::before {outline: thin dotted;outline: 5px auto -webkit-focus-ring-color;outline-offset: -2px; }
    		.checkbox input[type="checkbox"]:checked + label::after {font-family: 'FontAwesome';content: "\f00c"; }
    		.checkbox input[type="checkbox"]:disabled + label {opacity: 0.65; }
      		.checkbox input[type="checkbox"]:disabled + label::before {background-color: #eeeeee;cursor: not-allowed; }
  			.checkbox.checkbox-circle label::before {border-radius: 50%; }
  			.checkbox.checkbox-inline {margin-top: 0; }
  			
  			#ch_user1 {
			    width: 100%;
			    height: 32px;
			    border-radius: 4px;
			    margin-bottom: 10px;
			}
			
			.blockInfo{width: 15em; overflow: hidden; text-overflow: ellipsis;  white-space: nowrap; }
        </style>
        <!--[if lt IE 9]>
            <script src="html5shiv.min"></script>
        <![endif]-->    
    </head>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script>
    
    function addIncome_carNumber(carNumber,driverId){
    	document.getElementById("Incom_carNumber").value = carNumber;
    	document.getElementById("driverNationalId_income").value = driverId;
    }
    
    function addFine_carNumber(carNumber,driverId){
    	document.getElementById("Fine_carNumber").value = carNumber;
    	document.getElementById("driverNationalId_fine").value = driverId;
    }
    
    function addMentain_carNumber(carNumber){
    	document.getElementById("Maintain_carNumber").value = carNumber;
    }
    
    function checkDelete(carNumber){
    	if (confirm("are you sure you want to delete this car?") == true) {
//    		alert("hh");
          	window.location.href=("http://carsystemi-amrseproject.rhcloud.com/DeleteCar?carNumber="+carNumber);
    		/*var url = window.location.href;    
    		if (url.indexOf('?') > -1){
    		   url += 'http://carsystemi-amrseproject.rhcloud.com/DeleteCar&carNumber=1'
    		}else{
    		   url += 'http://carsystemi-amrseproject.rhcloud.com/DeleteCar?carNumber=1'
    		}
    		window.location.href = url;*/
        } 
    }
    function popUpUpdateCarContainerX(){
        document.getElementById("popUpUpdateCarContainer").style.display="none";
        document.getElementById("All").style.opacity=1;
    }
    function popUpUpdateCarContainer(i,carNumber,carStatus,driverNationalId ,countKm ,remainLoan ,paymentType ,imageURL ){
   
		document.getElementById("carNumber").placeholder = carNumber;
		document.getElementById("carNumber").value = carNumber;
		
		if(driverNationalId ==0){
			document.getElementById("carDriverNationalId").style.display = "none";
		}
		else{
			document.getElementById("carDriverNationalId").placeholder = driverNationalId;
			document.getElementById("carDriverNationalId").value = driverNationalId;
		}
		 
		 
		document.getElementById("countKm").placeholder = countKm;
		document.getElementById("countKm").value = countKm;
		
		document.getElementById("remainLoan").placeholder = remainLoan;
		document.getElementById("remainLoan").value = remainLoan;
		
		document.getElementById("carStatus").placeholder = carStatus;
		document.getElementById("carStatus").value = carStatus;
		
		document.getElementById("paymentType").placeholder = paymentType;
		document.getElementById("paymentType").value = paymentType;
		
		document.getElementById("imageURL").placeholder = imageURL;
		document.getElementById("imageURL").value = imageURL;
  }
    
    //----------
    function popUpAssignCarContainer(carNumber,carType,carModel,carCompanyName,driverNationalId){
//   		alert(carNumber);
		document.getElementById("assignCar_CarNumber").innerHTML  = carNumber;
		document.getElementById("carAssign_carNumber").value  = carNumber;
		document.getElementById("assignCar_CarModel").innerHTML  = carModel;
		document.getElementById("assignCar_CarType").innerHTML  = carType;
		document.getElementById("assignCar_CarCompanyName").innerHTML  = carCompanyName;
		
  }
    //-----------------
    function show_overlay(overlayDiv,assigned_not){	
    	if(assigned_not==0 || !assigned_not)
    		overlayDiv.children[2].children[0].children[0].style.display="block";
    	else 
    		overlayDiv.children[2].children[0].children[1].children[1].style.display="block";
            	
    	overlayDiv.children[2].style.height= "100%";
    	overlayDiv.children[2].style.bottom= "0";
    	
    	overlayDiv.addEventListener("mouseleave",function() {
    		overlayDiv.children[2].style.height= "0";
        	overlayDiv.children[2].style.bottom= "100%";
    	});
    }
    
    function getSelected(){
    	//alert("carDriverNationalId");
    	var yourSelect = document.getElementById("type" );
    	//alert( yourSelect.options[ yourSelect.selectedIndex ].value );
    	
    	document.getElementById("carDriverNationalId_addCar").value = yourSelect.options[ yourSelect.selectedIndex ].value;
    }
    function getSelected2(){
    	//alert("carDriverNationalId");
    	var yourSelect = document.getElementById("type2" );
    	//alert( yourSelect.options[ yourSelect.selectedIndex ].value );
    	
    	document.getElementById("carDriverNationalId_assigncar").value = yourSelect.options[ yourSelect.selectedIndex ].value;
    }
    /*********/

    /*********/
    function getSelectedCompanyName(){
    	var yourSelect = document.getElementById("companyNameSelet" );
    	//alert( yourSelect.options[ yourSelect.selectedIndex ].value );
    	
    	document.getElementById("companyName_addCar").value = yourSelect.options[ yourSelect.selectedIndex ].value;
    	
    	//alert("befor ajax");
    	$.ajax({
            type: "GET",
            url:"getTypeOfSelectedCompany",
            data: {companyName: yourSelect.options[ yourSelect.selectedIndex ].value},
            dataType: "json",
            success: function (data) { 
            	//alert("success ajax");
            	//alert(data);
            	$('#ch_user1').empty(); 
                $.each(data,function(i,obj)
                {
                 //alert(obj.carType+":"+obj.financeCompanyName);
                 $("#ch_user1").append("<option value='" + obj.carType + "'>" + obj.carType + "</option>");
                });
                //alert(div_data);
                //$(div_data).appendTo('#');   
                }
          });
    } 
    
    function assignDriver_addCar(){
    	if(document.getElementById("checkbox2").checked == true){
        	//alert("checked, Assign Driver");
        	document.getElementById("driverID_FormDIV").style.display="block";
        } 
        else{
        	//alert("not checked, DO NOT Assign");	
        	document.getElementById("driverID_FormDIV").style.display="none";
        }
    }
    //-----------------
    //----------- 
    </script>
    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-toggleable-md navbar-light bg-faded navbar-inverse">
              <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <a class="navbar-brand" href="homePage.jsp">Navbar</a>

              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="EmployeePage.jsp">Employee <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                  	<button type="button"  id="addCarItem" class="nav-link" data-toggle="modal" data-target="#AddCarModal">Add Car</button>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="carsMaintainance.jsp">Cars Maintenance <span class="sr-only">(current)</span></a>
                  </li>
                   <li class="nav-item">
                    <a class="nav-link" href="finaneCompany.jsp">Finance Companies <span class="sr-only">(current)</span></a>
                  </li>
                </ul>
                <ul class="navbar-nav mr-auto navbar-right">
                    <li class="nav-item">
                    	<form action="LogOut" method="post">
                    		<input type="submit" class="nav-item" id="logoutButton" value="Logout">
                    	</form> 
                    </li>
                </ul>
              </div>
            </nav>
        </div>
    
        <!--cards-->
        <div id="car_cards">
            <div class="card-group">
                <div class="row">
           
                <% ArrayList<CarModel> cars= (ArrayList<CarModel>)session.getAttribute("allCars");
            	if(cars == null || cars.size()==0){
            		CarModel obj = new CarModel();
            		cars = obj.getAllCars();
            	}
            	ArrayList<EmployeeModel> employees = (ArrayList<EmployeeModel>) session.getAttribute("allEmployees");
            	if(employees == null || employees.size()==0){
            		EmployeeModel obj = new EmployeeModel();
            		employees = obj.getAllEmployee();
            	}
            	ArrayList<FinanceCompanyModel> companies = (ArrayList<FinanceCompanyModel>) session.getAttribute("allCompanies");
            	if(companies == null || companies.size()==0){
            		FinanceCompanyModel obj = new FinanceCompanyModel();
            		companies = obj.getAllFinanceCompanies();
            	}
            	ArrayList<CompanyTypeModel> types = (ArrayList<CompanyTypeModel>) session.getAttribute("allTypes");
            	if(types == null || types.size()==0){
            		CompanyTypeModel typ_company = new CompanyTypeModel();
            		types = typ_company.getAllTypes();
            	}
            	
            	
            	String carNumber;
            	int driverNationalId;
            	double countKm,remainLoan;
            	String carStatus,paymentType,imageURL;
            	String carModel,carType,carCompanyName;
            	int assigned_not;
            	
            	
            	for(int i=0; i<cars.size(); i++){
            		carNumber = cars.get(i).getCarNumber();
            		driverNationalId = cars.get(i).getCarDriverNationalId();
            		countKm = cars.get(i).getCountKm();
            		remainLoan = cars.get(i).getRemainLoan();
            		carStatus = cars.get(i).getCarStatus();
            		paymentType = cars.get(i).getPaymentType();
            		imageURL = cars.get(i).getImageUrl();
            		carModel = cars.get(i).getModel();
            		carType = cars.get(i).getType();
            		carCompanyName = cars.get(i).getCarCompanyName();
            		assigned_not = cars.get(i).getassigned_not();
            		
            		%>
            		<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
            			<div class="card">
            								<!-- *hover the image* ... -->
		                	<div class="photo" onmouseover="show_overlay(this, <%=assigned_not %>)">
		                		<img src="RetrieveImage?id=<%=carNumber %>" name="name" width="100%" height="100%" alt="Card image cap">
		                		<div id="overlay">
	                                <div class="text">																															
	                                	<button id="assign_car"   class="assinged_notAssigned" data-toggle="modal" data-target="#AssignCar" onclick="popUpAssignCarContainer('<%=carNumber %>','<%=carType %>','<%=carModel %>','<%=carCompanyName %>',<%=driverNationalId %>)">Assign</button>
	                                	<form action="UnAssignDriverCar" method="post">
						               		<input type="hidden"  value=<%=cars.get(i).getCarNumber() %> name="carNumber" >
						                  	<input type="submit" id="assigned_car" class="btn btn-primary btn_car assinged_notAssigned" value="Un_assigned">
						                </form>
	                                </div> 
	                            </div>
		                	</div>
		                	<div class="card-body">
		                				<!-- *press title to go to car details* ... -->
			                  	<h4 class="card-title"><a href="#" class="alert-link"><%=cars.get(i).getType()%>, <%=cars.get(i).getModel() %></a></h4>
			                  
				                <div class="card-text blockInfo"> <label class="text-muted">Car Number: </label><p title=<%=cars.get(i).getCarNumber() %>> <%=cars.get(i).getCarNumber() %></p></div>
				                <div class="card-text blockInfo"> <label class="text-muted">Company Name: </label><p title=<%=cars.get(i).getCarCompanyName() %>> <%=cars.get(i).getCarCompanyName() %></p></div>
				                <div class="card-text blockInfo"> <label class="text-muted">Style: </label><p> <%=cars.get(i).getStyle() %></p></div>
				                <div class="card-text blockInfo"> <label class="text-muted">countKm: </label><p> <%=cars.get(i).getCountKm() %></p></div>
				                <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>  -->
			                </div>  
			                <div class="card-footer">
				                <form action="CarReport" method="post">
				               		<input type="hidden"  value=<%=cars.get(i).getCarNumber() %> name="carNumber" >
				                  	<input type="submit" class="btn btn-primary btn_car" value="Report">
				                </form>
			                  
			                    	<!-- *no need to driver info button, will be shown in the report* ... -->
			                    	<!-- <button type="button" class="btn btn-primary">Driver Info</button> -->
			                    
			                   <button type="button" class="btn btn-primary btn_car" data-toggle="modal" data-target="#updateCarModal"  onclick="popUpUpdateCarContainer(<%=i %>,'<%=carNumber %>','<%=carStatus %>',<%=driverNationalId %>,<%=countKm %>,<%=remainLoan %>,'<%=paymentType %>','<%=imageURL %>')">Update</button>
			                   <button type="button" class="btn btn-primary btn_main_dele" data-toggle="modal" data-target="#addMaintainance" onclick="addMentain_carNumber('<%=carNumber %>')"> Add Maintainance</button>
			                   <button type="button" class="btn btn-primary btn_car" data-toggle="modal" data-target="#addIncom" onclick="addIncome_carNumber('<%=carNumber %>',<%=driverNationalId%>)"> Add Income</button>
			                   <button type="button" class="btn btn-primary btn_car" data-toggle="modal" data-target="#addFine"  onclick="addFine_carNumber('<%=carNumber %>',<%=driverNationalId%>)"> Add Fine</button>
			                   
			                   <button type="button" class="btn_main_dele btn btn-danger" onclick="checkDelete('<%=cars.get(i).getCarNumber()%>')">Delete</button>
			                    <!--  <form action="DeleteCar" method="post">
			                    	<input type="hidden" class="btn btn-danger" value=<%=cars.get(i).getCarNumber() %> name="carNumber">
			                    	<input type="submit" value="Delete">
			                    </form> -->
			                </div> 
		                </div>
		           </div>
            	<% } %>
            </div>
        </div>  
        </div>
        <!--cards-->
        <!-- start modal -->
	        	<!-- Button trigger modal -->
			
			<!-- Modal -->
			<div class="modal fade" id="updateCarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <!-- ***************** -->
                        <!--start update Car-->
					        <form action="UpdateCar" method="post" enctype="multipart/form-data">
							  <div class="form-group">
							    <input class="form-control" name="carNumber" type="hidden" name="carNumber" id="carNumber">
							  </div>
							  <div class="form-group">
							    <!-- <label class="form-control-label">Driver NationalId</label>-->
							    <input class="form-control" name="carDriverNationalId" type="hidden" id="carDriverNationalId">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">CountKm</label>
							    <input class="form-control" name="countKm" type="text" id="countKm">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">RemainLoan</label>
							    <input class="form-control" name="remainLoan" type="text" id="remainLoan">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">CarStatus</label>
							    <input class="form-control" name="carStatus" type="text" id="carStatus">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">PaymentType</label>
							    <input class="form-control" name="paymentType" type="text" id="paymentType">
							  </div>
							  <!-- <div class="form-group">
							    <label class="form-control-label">Upload Image</label>
							    <input type="file" class="form-control" name="photo" id="fileUpload_updateCar" accept="image/jpeg" />
							  </div>-->
							  <!-- <div class="form-group">
							    <label class="form-control-label">imageUrl</label>
							    <input class="form-control" name="imageUrl" type="text" id="imageURL">
							  </div> -->
							  <div class="modal-footer">
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						        <input type="submit" class="btn btn-primary" value="Update">
						      </div>
							</form>
						<!--end update Car-->
                    <!-- ***************** -->
			      </div>
			    </div>
			  </div>
			</div>
        <!-- end Modal -->
        <!-- ============================================================== -->
        <!-- Add Mentainance -->
        <!-- Modal -->
			<div class="modal fade" id="addMaintainance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Add Maintainance</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <!-- ***************** -->
                        <!--start add Maintainance-->
					        <form action="AddMaintainance" method="post">
							  <div class="form-group">
							    <input class="form-control" name="carNumber" type="hidden" id="Maintain_carNumber">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Car Company</label>
							    <input class="form-control" name="carCompany" type="text"  id="carCompany" required>
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Next Service Date</label>
							    <input class="form-control" name="nextServiceDate" type="date" value="0000-00-00"  id="nextServiceDate" required>
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Services Cost</label>
							    <input class="form-control" name="serviceCost" type="text"   id="serviceCost" required>
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Service Date</label>
							    <input class="form-control" name="serviceDate" type="date" value="0000-00-00"  id="serviceDate" required>
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Service Type</label>
							    <input class="form-control" name="serviceType" type="text"  id="serviceType" required>
							  </div>
							  <div class="modal-footer">
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						        <input type="submit" class="btn btn-primary" value="Add">
						      </div>
							</form>
						<!--end update Car-->
                    <!-- ***************** -->
			      </div>
			    </div>
			  </div>
			</div>
        <!-- end Modal -->
        <!-- end Maintqinance -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Add Incom -->
        <!-- Modal -->
			<div class="modal fade" id="addIncom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Add Incom</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <!-- ***************** -->
                        <!--start Add Incom-->
					        <form action="AddCarIncome" method="post">
							  <div class="form-group">
							    <input class="form-control" name="carNumber" type="hidden"  id="Incom_carNumber">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Date</label>
							    <input class="form-control" name="date" type="date" value="0000-00-00"  id="date" required>
							  </div>
							  <div class="form-group">
							    <!-- <label class="form-control-label">Driver National Id</label> -->
							    <input class="form-control" name="driverNationalId" type="hidden" id="driverNationalId_income">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Income Type</label>
							    <input class="form-control" name="incomType" type="text" id="incomType" required>
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Revenue</label>
							    <input class="form-control" name="revenue" type="text"  id="revenue" required>
							  </div>
							  <div class="modal-footer">
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						        <input type="submit" class="btn btn-primary" value="Add">
						      </div>
							</form>
						<!--end Add Incom-->
                    <!-- ***************** -->
			      </div>
			    </div>
			  </div>
			</div>
        <!-- end Modal -->
        <!-- end Add Incom -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Add Fine -->
        <!-- Modal -->
			<div class="modal fade" id="addFine" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Add Fine</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <!-- ***************** -->
                        <!--start Add Fine-->
					        <form action="AddCarFine" method="post">
							  <div class="form-group">
							    <input class="form-control" name="carNumber" type="hidden"  id="Fine_carNumber" >
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Description</label>
							    <input class="form-control" name="description" type="text" id="description" required>
							  </div>
							  <div class="form-group">
							    <!-- <label class="form-control-label">Driver National Id</label> -->
							    <input class="form-control" name="driverNationalId" type="hidden"  id="driverNationalId_fine">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Fine Amount</label>
							    <input class="form-control" name="fineAmount" type="text" id="fineAmount" required>
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Fine Date</label>
							    <input class="form-control" name="fineDate" type="date" value="0000-00-00"  id="fineDate" required>
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Fine Type</label>
							    <input class="form-control" type="text" id="fineType" required>
							  </div>
							  <div class="modal-footer">
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						        <input type="submit" class="btn btn-primary" value="Add">
						      </div>
							</form>
						<!--end Add Fine-->
                    <!-- ***************** -->
			      </div>
			    </div>
			  </div>
			</div>
        <!-- end Modal -->
        <!-- end Add Fine -->
        <!-- ============================================================== -->
        <!-- Start Add Car -->
       		<!-- Modal -->
			<div class="modal fade" id="AddCarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Add New Car</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true" class="closeModal">&times;</span>
			        </button>
			      </div> 
			      <div class="modal-body">
			        <!-- ****************** -->
			 
                        <!--start Add Car-->
					        <form action="AddNewCar" method="post" enctype="multipart/form-data">
							  <div class="form-group">
							  	<label class="form-control-label">Car Number</label>
							    <input class="form-control" name="carNumber" type="text" id="carNumber" required>
							  </div>
							  <div class="form-group">
							  	<label class="form-control-label">model</label>
							    <input class="form-control" name="model" type="text" id="model">
							  </div>
							  <div class="form-group">
							  	<label class="form-control-label">style</label>
							    <input class="form-control" name="style" type="text" id="style">
							  </div>
							  <div class="form-group">
							  	<label class="form-control-label">color</label>
							    <input class="form-control" name="color" type="text" id="color">
							  </div>
							  <div class="form-group checkbox checkbox-primary">
							  	<input name="color" type="checkbox" id="checkbox2" onchange="assignDriver_addCar()">
							  	<label class="form-control-label" for="checkbox2">Assign Driver? </label>
							  </div>
							  <!--<div class="form-group">
							    <label class="form-control-label">Driver NationalId</label>
							    <input class="form-control" name="carDriverNationalId" type="text"  id="carDriverNationalId">
							  </div>
							  -->
							  <div class="form-group" id="driverID_FormDIV">
							    <label class="form-control-label">Driver NationalId</label><br>
							    <select name="type" id="type" onchange="getSelected()">
							    	<%for(int i=0; i<employees.size(); i++){
							    		EmployeeModel objEmp = new EmployeeModel();
							    		objEmp = employees.get(i); %>
							    		<option name="NID" value=<%=objEmp.getNationalId() %> class="options" selected="selected"><%=objEmp.getName() %> with National ID <%=objEmp.getNationalId() %></option>
							    	<%} %>
                    			</select>
							  </div>
							  
							  <div class="form-group">
							    <input class="form-control" name="carDriverNationalId_addCar" type="hidden"  id="carDriverNationalId_addCar">
							  </div>
							  
							  <div class="form-group">
							    <label class="form-control-label">CountKm</label>
							    <input class="form-control" name="countKm" type="text" id="countKm">
							  </div> 
							  <div class="form-group">
							    <label class="form-control-label">Monthly Installment</label>
							    <input class="form-control" name="monthlyInstallment" type="text" id="monthlyInstallment">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">Total Loan</label>
							    <input class="form-control" name="totalLoan" type="text" id="totalLoan">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">RemainLoan</label>
							    <input class="form-control" name="remainLoan" type="text" id="remainLoan">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">First Installment Date</label>
							    <input class="form-control" type="date" value="0000-00-00" name="firstInstallmentDate" id="firstInstallmentDate">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">CarStatus</label>
							    <input class="form-control" name="carStatus" type="text" id="carStatus">
							  </div>
							  <div class="form-group">
							    <label class="form-control-label">PaymentType</label>
							    <input class="form-control" name="paymentType" type="text" id="paymentType">
							  </div>
							  <!-- <div class="form-group">
							    <label class="form-control-label">Car Company Name</label>
							    <input class="form-control" name="carCompanyName" type="text"  id="carCompanyName">
							  </div>
							  -->
							  <div class="form-group">
							    <label class="form-control-label">Car Company Name</label><br>
							    <select name="companyNameSelet" id="companyNameSelet" onchange="getSelectedCompanyName()">
							    	<%for(int i=0; i<companies.size(); i++){
							    		FinanceCompanyModel objCompany = new FinanceCompanyModel();
							    		objCompany = companies.get(i); %>
							    		<option value=<%=objCompany.getCompanyName() %> class="options" selected="selected"><%=objCompany.getCompanyName() %></option>
							    	<%} %>
                    			</select>
							  </div> 
							  <div class="form-group">
							    <input class="form-control" name="companyName_addCar" type="hidden"  id="companyName_addCar">
							  </div>
							  
							  <div class="form-group">
							  	  <label class="form-control-label">Type</label>
								  <select id="ch_user1" >
							        
							      </select>
							  </div>
						    
							  <!-- <div class="form-group">
							  	<label class="form-control-label">Type</label>
							    <input class="form-control" name="type" type="text" id="Type">
							  </div>-->
							   
							  <div class="form-group">
							    <label class="form-control-label">Upload Image</label>
							    <input type="file" class="form-control" name="photo" id="fileUpload" required accept="image/jpeg" />
							  </div> <!--
							  <div class="form-group">
							    <label class="form-control-label">imageUrl</label>
							    <input class="form-control" name="imageUrl" type="text"  id="imageURL">
							  </div>-->
							  <div class="form-group">
							    <label class="form-control-label">Total Fines</label>
							    <input class="form-control" name="totalFines" type="text" name="totalFines" id="totalFines">
							  </div>
							  <div class="modal-footer">
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						        <input type="submit" class="btn btn-primary" value="Add">
						      </div>
							</form>
						<!--end Add Car-->
                    <!-- ***************** -->
			      </div>
			    </div>
			  </div>
			</div>
        <!-- end Modal -->
        <!-- End Add Car -->
        <!-- ============================================================= -->
        <!-- ============================================================== -->
        <!-- AssignCar -->
        <!-- Modal -->
			<div class="modal fade" id="AssignCar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Assign Car</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <!-- ***************** -->
                        <!--start AssignCar-->
					        <form action="AssignCarDriver" method="post">					   
						        	<div class="card-text blockInfo"> <label class="form-control-label text-muted">Car Number: </label> <p id="assignCar_CarNumber"></p></div>
							    <div class="form-group">    
							        <input class="form-control" name="carAssign_carNumber" type="hidden" id="carAssign_carNumber">
							    </div>
						        <!--<div class="form-group">
								  	<label class="form-control-label">Car Number</label>
								    <input class="form-control" name="carNumber" type="text" id="carNumber">
								</div>-->
								
								<div class="card-text blockInfo"> <label class="text-muted">Type: </label> <p id="assignCar_CarType"></p></div>
								<!--<div class="form-group">
								  	<label class="form-control-label">Type</label>
								    <input class="form-control" name="type" type="text" id="Type">
								</div>-->
								
								<div class="card-text blockInfo"> <label class="text-muted">Model: </label> <p id="assignCar_CarModel"></p></div>
								<!--<div class="form-group">
								  	<label class="form-control-label">model</label>
								    <input class="form-control" name="model" type="text" id="model">
								</div>-->
								
								<div class="card-text blockInfo"> <label class="text-muted">Company Name: </label> <p id="assignCar_CarCompanyName"></p></div>
								<!--<div class="form-group">
								    <label class="form-control-label">Car Company Name</label>
								    <input class="form-control" name="carCompanyName" type="text"  id="carCompanyName">
								</div>-->
								
								<div class="form-group" id="driverID_FormDIV2">
							    	<label class="form-control-label">Driver NationalId</label><br>
							    	<select name="type" id="type2" onchange="getSelected2()">
							    		<%for(int i=0; i<employees.size(); i++){
							    			EmployeeModel objEmp = new EmployeeModel();
							    			objEmp = employees.get(i); %>
							    			<option name="NID" value=<%=objEmp.getNationalId() %> class="options" selected="selected"><%=objEmp.getName() %> with National ID <%=objEmp.getNationalId() %></option>
							    		<%} %>
                    				</select>
							  	</div>
							  
								<div class="form-group">
								    <input class="form-control" value=<%=employees.get(0).getNationalId() %> name="assignCar_nationalId" type="hidden"  id="carDriverNationalId_assigncar">
								</div>
								<!-- 
								<div class="form-group">
								    <label class="form-control-label">Employee National ID </label>
								    <input class="form-control" name="assignCar_nationalId" type="text"  id="assignCar_nationalId">
								</div> -->
								<div class="modal-footer">
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							        <input type="submit" class="btn btn-primary" value="Add">
							    </div>
							</form>
						<!--end AssignCar--> 
                    <!-- ***************** -->
			      </div>
			    </div>
			  </div>
			</div>
        <!-- end AssignCar -->
        <!-- end AssignCar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        
        <!--Footer-->
        
        <!--End Footer-->
    </body>
</html>