<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/bootstrap.css">
        
        <title></title>
        <style>
            body,.container-fluid{margin: 0;padding: 0}
            .container-fluid{background-color: #222222;}
            .navbar{margin: 0} 
            .navbar-inverse{background-color: #1F2D3A}
            .navbar-inverse .navbar-nav > li > a{color: #e4dfdf}
            .navbar-inverse .navbar-brand{color: #fffefe}
            .slider-caption{text-align: center;word-wrap: 10px;background: rgba(0,0,0,0.8);padding: 10px; position: absolute;bottom: 205px ; right: 654px; width: 389px;padding-left: 15px;}
            .lead{font-size: 18px;text-align: left;line-height: 25px;letter-spacing: 1px;}
            .carousel-inner > .item > img, .carousel-inner > .item > a > img{width: 1519px;}
            #LogInContainer{text-align: center;border-radius: 4px;background-color: #F0F8FF; display: none;width: 400px;height: 260px;padding: 40px;position: fixed;z-index: 1;left: 0;top: 0; margin-left: 550px;margin-top: 160px;padding: 40px;z-index: 1;left: 0;top: 0;overflow: auto;padding-bottom: 20px}
            #Logbutton{width: 514px;height: 40px;padding: 10px;border: none;color: white}
            .close{color: black;font-size: 20px;display: inline;;position: relative;top: 5px;left: 17px}
            .close{cursor: pointer;color: black;font-weight: bold;margin-left: 340px;position: relative;top: -25px;font-size: 20px}
            .animate {
                    -webkit-animation: animatezoom 0.6s;
                    animation: animatezoom 0.6s
                }

                @-webkit-keyframes animatezoom {
                    from {-webkit-transform: scale(0)} 
                    to {-webkit-transform: scale(1)}
                }

                @keyframes animatezoom {
                    from {transform: scale(0)} 
                    to {transform: scale(1)}
                }

                /* Change styles for span and cancel button on extra small screens */
                @media screen and (max-width: 300px) {
                    span.psw {
                       display: block;
                       float: none;
                    }
                    .cancelbtn {
                       width: 100%;
                    }
                }
            #sign-up-form{padding: 10px;width: 330px;margin-bottom: 10px;margin-top: 0}
            #sign-up-submit{padding: 12px 40px;color: white;background-color: #2754EB;border: none;box-shadow: 2px 3px 5px #2754EB;cursor: pointer;margin-top: 5px}
            #sign-up-submit:hover{background-color: #323232;box-shadow: 2px 3px 5px #323232}
            /*===============================*/
            @import url(http://fonts.googleapis.com/css?family=Roboto);

/****** LOGIN MODAL ******/
.loginmodal-container {
  padding: 30px;
  max-width: 350px;
  width: 100% !important;
  background-color: #F7F7F7;
  margin: 0 auto;
  border-radius: 2px;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
  overflow: hidden;
  font-family: roboto;
}

.loginmodal-container h1 {
  text-align: center;
  font-size: 1.8em;
  font-family: roboto;
}

.loginmodal-container input[type=submit] {
  width: 100%;
  display: block;
  margin-bottom: 10px;
  position: relative;
}

.loginmodal-container input[type=text], input[type=password] {
  height: 44px;
  font-size: 16px;
  width: 100%;
  margin-bottom: 10px;
  -webkit-appearance: none;
  background: #fff;
  border: 1px solid #d9d9d9;
  border-top: 1px solid #c0c0c0;
  /* border-radius: 2px; */
  padding: 0 8px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
}

.loginmodal-container input[type=text]:hover, input[type=password]:hover {
  border: 1px solid #b9b9b9;
  border-top: 1px solid #a0a0a0;
  -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
}

.loginmodal {
  text-align: center;
  font-size: 14px;
  font-family: 'Arial', sans-serif;
  font-weight: 700;
  height: 36px;
  padding: 0 8px;
/* border-radius: 3px; */
/* -webkit-user-select: none;
  user-select: none; */
}

.loginmodal-submit {
  /* border: 1px solid #3079ed; */
  border: 0px;
  color: #fff;
  text-shadow: 0 1px rgba(0,0,0,0.1); 
  background-color: #4d90fe;
  padding: 17px 0px;
  font-family: roboto;
  font-size: 14px;
  /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#4787ed)); */
}

.loginmodal-submit:hover {
  /* border: 1px solid #2f5bb7; */
  border: 0px;
  text-shadow: 0 1px rgba(0,0,0,0.3);
  background-color: #357ae8;
  /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#357ae8)); */
}

.loginmodal-container a {
  text-decoration: none;
  color: #666;
  font-weight: 400;
  text-align: center;
  display: inline-block;
  opacity: 0.6;
  transition: opacity ease 0.5s;
} 

.login-help{
  font-size: 12px;
}

.navbar{height: 50px}
a#lobot {
    text-decoration: none;
        position: absolute;
    right: 0;
    top: -5px;
}
.navbar-brand{position: absolute;
    top: -4px;}
            /*===============================*/
        </style>
        <!--[if lt IE 9]>
            <script src="html5shiv.min"></script>
        <![endif]-->    
    </head>
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
        function toDo2(){
                
                document.getElementById("lobot").style.display="none";
                document.getElementById("LogInContainer").style.display="block";
                document.getElementById("signup").style.display="none";
                document.getElementById("All").style.opacity=0.3;
            }
         function nothing(){
                
                document.getElementById("lobot").style.display="inline";
                document.getElementById("LogInContainer").style.display="none";
                document.getElementById("signup").style.display="inline";
                document.getElementById("All").style.opacity=1;
            }
    </script>
    <body>  
        <div id="All">
        <div class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#login-modal" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="login.jsp">Brand</a>
                    </div>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto navbar-right">
                            <li><a href="#" id="lobot" data-toggle="modal" data-target="#login-modal">LogIn</a></li>
                        </ul>
                      </div>
                </div>
            </div>
        
        <div class="container-fluid">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                      <img src="images/back1.jpg">
                      <div class="carousel-caption">
                        <div class="slider-caption">
                          <p class="lead">Write Paragraph caption, write Paragraph caption, write Paragraph caption
                          write Paragraph caption write Paragraph caption write Paragraph caption </p>
                        </div>
                      </div>
                    </div>
                    <div class="item">
                      <img src="images/back2.jpg">
                      <div class="carousel-caption">
                        <div class="slider-caption">
                          <p class="lead">Write Paragraph caption, write Paragraph caption, write Paragraph caption
                          write Paragraph caption write Paragraph caption write Paragraph caption</p>
                        </div>
                      </div>
                    </div>
                    <div class="item">
                      <img src="images/back3.jpg">
                      <div class="carousel-caption">
                        <div class="slider-caption">
                          <p class="lead">Write Paragraph caption, write Paragraph caption, write Paragraph caption
                          write Paragraph caption write Paragraph caption write Paragraph caption</p>
                        </div>
                      </div>
                    </div>
                    
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        </div>
        <!--start LogIn-->
        <div id="LogInContainer">
            <span onclick="nothing()" class="close" title="Close Modal">&times;</span>
            <form action="LogIn" method="post" class="animate">
                <input type="text" name="nationalId" placeholder="email" id="sign-up-form">
                <input type="password" name="password" placeholder="password" id="sign-up-form">
                <input type="submit" value="LOGIN" id="sign-up-submit" >
            </form><br>
            
        </div>
        <!-- ====================================== -->
        <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    	  <div class="modal-dialog">
				<div class="loginmodal-container">
					<h1>Login to Your Account</h1><br>
				  <form action="LogIn" method="post">
					<input type="text" name="nationalId" placeholder="email" >
					<input type="password" name="password" placeholder="password">
					<input type="submit" name="login" class="login loginmodal-submit" value="Login">
				  </form>
					
				  
				</div>
			</div>
		  </div>
        <!--end LogIn-->
        

        
    </body>
</html>